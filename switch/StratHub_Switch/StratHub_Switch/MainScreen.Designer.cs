﻿namespace StratHub_Switch
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBrightness = new System.Windows.Forms.Label();
            this.lblTemperature = new System.Windows.Forms.Label();
            this.trackBarBrightness = new System.Windows.Forms.TrackBar();
            this.trackBarTempChange = new System.Windows.Forms.TrackBar();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.bottomLayout = new System.Windows.Forms.TableLayoutPanel();
            this.btnOnOff = new System.Windows.Forms.PictureBox();
            this.lnkChangeColor = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBrightness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTempChange)).BeginInit();
            this.panelBottom.SuspendLayout();
            this.bottomLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnOnOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lnkChangeColor)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBrightness
            // 
            this.lblBrightness.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBrightness.AutoSize = true;
            this.lblBrightness.Location = new System.Drawing.Point(154, 0);
            this.lblBrightness.Name = "lblBrightness";
            this.lblBrightness.Size = new System.Drawing.Size(37, 30);
            this.lblBrightness.TabIndex = 5;
            this.lblBrightness.Text = "0 %";
            this.lblBrightness.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTemperature
            // 
            this.lblTemperature.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTemperature.AutoSize = true;
            this.lblTemperature.Location = new System.Drawing.Point(3, 0);
            this.lblTemperature.Name = "lblTemperature";
            this.lblTemperature.Size = new System.Drawing.Size(42, 30);
            this.lblTemperature.TabIndex = 6;
            this.lblTemperature.Text = "1500 k";
            this.lblTemperature.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBarBrightness
            // 
            this.trackBarBrightness.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.trackBarBrightness.Dock = System.Windows.Forms.DockStyle.Right;
            this.trackBarBrightness.Location = new System.Drawing.Point(239, 0);
            this.trackBarBrightness.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.trackBarBrightness.Maximum = 100;
            this.trackBarBrightness.Name = "trackBarBrightness";
            this.trackBarBrightness.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarBrightness.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.trackBarBrightness.RightToLeftLayout = true;
            this.trackBarBrightness.Size = new System.Drawing.Size(45, 261);
            this.trackBarBrightness.TabIndex = 7;
            this.trackBarBrightness.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarBrightness.ValueChanged += new System.EventHandler(this.brightnessChange_ValueChanged);
            this.trackBarBrightness.MouseCaptureChanged += new System.EventHandler(this.brightnessChange_Scroll);
            // 
            // trackBarTempChange
            // 
            this.trackBarTempChange.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.trackBarTempChange.Dock = System.Windows.Forms.DockStyle.Left;
            this.trackBarTempChange.Location = new System.Drawing.Point(0, 0);
            this.trackBarTempChange.Maximum = 9000;
            this.trackBarTempChange.Minimum = 1500;
            this.trackBarTempChange.Name = "trackBarTempChange";
            this.trackBarTempChange.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarTempChange.Size = new System.Drawing.Size(45, 261);
            this.trackBarTempChange.TabIndex = 8;
            this.trackBarTempChange.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarTempChange.Value = 1500;
            this.trackBarTempChange.ValueChanged += new System.EventHandler(this.tempChange_ValueChanged);
            this.trackBarTempChange.MouseCaptureChanged += new System.EventHandler(this.tempChange_Scroll);
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.bottomLayout);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(45, 231);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(194, 30);
            this.panelBottom.TabIndex = 9;
            // 
            // bottomLayout
            // 
            this.bottomLayout.AutoSize = true;
            this.bottomLayout.ColumnCount = 3;
            this.bottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.79191F));
            this.bottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.20809F));
            this.bottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.bottomLayout.Controls.Add(this.lblTemperature, 0, 0);
            this.bottomLayout.Controls.Add(this.lblBrightness, 2, 0);
            this.bottomLayout.Controls.Add(this.lnkChangeColor, 1, 0);
            this.bottomLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomLayout.Location = new System.Drawing.Point(0, 0);
            this.bottomLayout.Name = "bottomLayout";
            this.bottomLayout.RowCount = 1;
            this.bottomLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.bottomLayout.Size = new System.Drawing.Size(194, 30);
            this.bottomLayout.TabIndex = 0;
            // 
            // btnOnOff
            // 
            this.btnOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnOnOff.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_0w;
            this.btnOnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOnOff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOnOff.Location = new System.Drawing.Point(45, 0);
            this.btnOnOff.Name = "btnOnOff";
            this.btnOnOff.Size = new System.Drawing.Size(194, 231);
            this.btnOnOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnOnOff.TabIndex = 10;
            this.btnOnOff.TabStop = false;
            this.btnOnOff.Click += new System.EventHandler(this.btnOnOff_Click);
            // 
            // lnkChangeColor
            // 
            this.lnkChangeColor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkChangeColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lnkChangeColor.Image = global::StratHub_Switch.Properties.Resources._382px_BYR_color_wheel;
            this.lnkChangeColor.Location = new System.Drawing.Point(51, 3);
            this.lnkChangeColor.Name = "lnkChangeColor";
            this.lnkChangeColor.Size = new System.Drawing.Size(97, 24);
            this.lnkChangeColor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lnkChangeColor.TabIndex = 7;
            this.lnkChangeColor.TabStop = false;
            this.lnkChangeColor.Click += new System.EventHandler(this.lnkChangeColor_LinkClicked);
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnOnOff);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.trackBarTempChange);
            this.Controls.Add(this.trackBarBrightness);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainScreen";
            this.Text = "MainScreen";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBrightness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTempChange)).EndInit();
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.bottomLayout.ResumeLayout(false);
            this.bottomLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnOnOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lnkChangeColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblBrightness;
        private System.Windows.Forms.Label lblTemperature;
        private System.Windows.Forms.TrackBar trackBarBrightness;
        private System.Windows.Forms.TrackBar trackBarTempChange;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.TableLayoutPanel bottomLayout;
        private System.Windows.Forms.PictureBox btnOnOff;
        private System.Windows.Forms.PictureBox lnkChangeColor;
    }
}