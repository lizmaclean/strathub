﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cyotek.Windows.Forms;
using StratHub_Base; 

namespace StratHub_Switch
{
    /// <summary>
    /// Color Picker class based on 3.2" raspberry pi touch screen 
    /// </summary>
    public partial class HardColorPicker : BaseForm
    {
        private StratHub_Base.StratHubBase hub_base;
        private static string light_key = "d073d52360de";
        
        private HardMainScreen mainScreen; 
        
<<<<<<< HEAD
        public HardColorPicker(StratHub_Base.StratHubBase hub_base, HardMainScreen mainScreen)
=======
        /// <summary>
        /// Color Picker screen based on the 3.2" Raspberry Pi Touch Screen 
        /// </summary>
        /// <param name="hub_base"></param>
        /// <param name="mainScreen"></param>
        public HardColorPicker(StratHub_Base.StratHubBase hub_base, HardMainScreen mainScreen, string lightId)
>>>>>>> macco
        {
            this.hub_base = hub_base;
            light_key = lightId; 
            this.mainScreen = (HardMainScreen)mainScreen; 

            InitializeComponent();
        }
        
        /// <summary>
        /// Event handler on what to do when the color is changes. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void colorSlider_ColorChanged(object sender, EventArgs e)
        {
<<<<<<< HEAD
            panel1.BackColor = colorWheel.Color;

            SetButtonColor();

            hub_base.ChangeColor(light_key, (int)colorWheel.Color.GetHue(), colorWheel.Color.GetSaturation(), 5000); 
            //hub_base.ChangeColor(light_key, colorWheel.Color.R, colorWheel.Color.G, colorWheel.Color.B, 5000);
=======
            var index = (int)((ColorSlider)sender).Value;
            var colors = colorSlider.CustomColors;
            var rgbColor = colors[index];
            
            SetButtonColor(rgbColor);
            hub_base.ChangeColor(light_key, rgbColor.R, rgbColor.G, rgbColor.B, true);
>>>>>>> macco
        }

        /// <summary>
        /// /// Sets the background color of the mainscreen on/off button
        /// </summary>
        /// <param name="color"></param>
        internal void SetButtonColor(Color color)
        {
            mainScreen.ChangeButtonColor(color); 
        }

        /// <summary>
        /// Go back to main screen 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblBack_LinkClicked(object sender, EventArgs e)
        {
            mainScreen.Visible = true;
            this.Close();
        }
    }
}
