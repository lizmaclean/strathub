﻿using StratHub_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StratHub_Switch
{
    public partial class HardLightSelection : Form
    {
        private StratHub_Base.StratHubBase stratHub_Base;
        private Caller caller; 

<<<<<<< HEAD
=======
        /// <summary>
        /// Light selection Constructor
        /// </summary>
        /// <param name="hub_Base"></param>
        /// <param name="caller"></param>
>>>>>>> macco
        public HardLightSelection(StratHub_Base.StratHubBase hub_Base, Caller caller)
        {
            stratHub_Base = hub_Base;

            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

            List<string> lightIds = stratHub_Base.GetAllLights(); 
            //List<string> lanLightIds = stratHub_Base.GetAllLANLights();
            //List<string> httpLightIds = stratHub_Base.GetAllHTTPLights();
            //List<string> lightIds = httpLightIds;  
            this.caller = caller; 

            InitializeComponent();
            
            foreach (var id in lightIds)
            {
                CreateNewLightPictureBox(id); 
            }
        }

        /// <summary>
        /// Creates a new picture box for every available light 
        /// </summary>
        /// <param name="lightId"></param>
        private void CreateNewLightPictureBox(string lightId)
        {
            PictureBox lightBox = new PictureBox();
            ((System.ComponentModel.ISupportInitialize)(lightBox)).BeginInit();
            lightBox.Name = lightId;
            lightBox.Size = new Size(60, 90);
            lightBox.BackColor = System.Drawing.SystemColors.Control;
            lightBox.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_0w;
            lightBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            lightBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            lightBox.TabStop = false;
            lightBox.Click += LightBox_Click;

            Label lblId = new Label();
            lblId.Name = "lbl:" + lightId;
            lblId.Text = lightId;
            lblId.Size = new Size(60, 30);
            lblId.AutoSize = true;
            lblId.Anchor = AnchorStyles.None;
            lblId.Left = (lightBox.Width - lblId.Width) / 2;
            lblId.Top = (lightBox.Height - lblId.Height) / 2;
            lblId.Click += LightBox_Click;
            lightBox.Controls.Add(lblId); 
            pnlLightFlow.Controls.Add(lightBox);
            pnlLightFlow.Show(); 
        }

        /// <summary>
        /// On click navigate to specified light 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LightBox_Click(object sender, EventArgs e)
        {
            string id = null; 
            Control clickedControl = (Control)sender; 
            if (clickedControl.Name.Contains("lbl:"))
            {
                id = clickedControl.Name.Split(':')[1];
            }
            else
            {
                id = clickedControl.Name; 
            }

            HardMainScreen mainScreen = new HardMainScreen(this, stratHub_Base, caller, id, null);
<<<<<<< HEAD
            this.Hide();
            mainScreen.ShowDialog();
=======
            mainScreen.Show(); 
            mainScreen.BringToFront();
            this.Hide();
>>>>>>> macco
        }
        
        /// <summary>
        /// Helps with certificates with older versions of Mono
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        public bool MyRemoteCertificateValidationCallback(System.Object sender,
            X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            // If there are errors in the certificate chain,
            // look at each error to determine the cause.
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                for (int i = 0; i < chain.ChainStatus.Length; i++)
                {
                    if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        continue;
                    }
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                        break;
                    }
                }
            }
            return isOk;
        }

        public bool MyRemoteCertificateValidationCallback(System.Object sender,
            X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            // If there are errors in the certificate chain,
            // look at each error to determine the cause.
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                for (int i = 0; i < chain.ChainStatus.Length; i++)
                {
                    if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        continue;
                    }
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                        break;
                    }
                }
            }
            return isOk;
        }
    }
}
