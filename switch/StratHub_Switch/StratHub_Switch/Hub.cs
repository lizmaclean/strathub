﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Windows.Forms;
using StratHub_Base;

namespace StratHub_Switch
{
    /// <summary>
    /// Main Hub Class
    /// </summary>
    static class Hub
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        private static StratHub_Base.StratHubBase hub_base;
        private static string token = "c4ee25616303ac1c55af8037fafe08ed03064813fc0acc0d3d9a01683df18f3c";
        private static string light_id = "d073d52360de";
        private static string light_key = "d073d52360de";

        /// <summary>
        /// Main Thread to start the program 
        /// </summary>
        [STAThread]
        static void Main()
        {
            Initialize(); 
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
<<<<<<< HEAD

            //TODO: Setup this up further
            //Application.Run(new SetupMainHard());

            var caller = new Caller();
            var relay = caller.CallPython("RelayPin.py");

            //After setup is completed
=======
            var caller = new Caller();
            var relay = caller.CallPython("RelayPin.py");
>>>>>>> macco
            Application.Run(new HardLightSelection(hub_base, caller)); 
        }
        
        /// <summary>
        /// Initialize Method to start API 
        /// </summary>
        public static void Initialize()
        {
<<<<<<< HEAD
<<<<<<< HEAD
            hub_base = StratHub_Base.StratHub_Base.GetInstance(token);
=======
            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
            hub_base = StratHub_Base.StratHubBase.GetInstance(token);
>>>>>>> a518705438121fa6635be518bef3014c9e5436ae
=======
            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
            hub_base = StratHub_Base.StratHubBase.GetInstance(token);
>>>>>>> macco

            //hub_base = new StratHub_Base.StratHub_Base.GetInstance(); 
        }
        
        /// <summary>
        /// Helps with certificates with older versions of Mono
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        public static bool MyRemoteCertificateValidationCallback(System.Object sender,
            X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            // If there are errors in the certificate chain,
            // look at each error to determine the cause.
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                for (int i = 0; i < chain.ChainStatus.Length; i++)
                {
                    if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        continue;
                    }
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                        break;
                    }
                }
            }
            return isOk;
        }

        public static bool MyRemoteCertificateValidationCallback(System.Object sender,
            X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            // If there are errors in the certificate chain,
            // look at each error to determine the cause.
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                for (int i = 0; i < chain.ChainStatus.Length; i++)
                {
                    if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        continue;
                    }
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                        break;
                    }
                }
            }
            return isOk;
        }
    }
}
