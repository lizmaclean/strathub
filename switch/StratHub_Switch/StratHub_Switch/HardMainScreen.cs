﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StratHub_Base;

namespace StratHub_Switch
{
    /// <summary>
    /// Main Screen HardCoded to Raspberry Pi 3.2" screen 
    /// </summary>
    public partial class HardMainScreen : Form
    {
        private StratHub_Base.StratHubBase hub_base;
        private static string lightId = "d073d52360de";
        private static Dictionary<string, object> status;

        HardLightSelection hardLightSelection; 
        
        private Caller caller;
        private Process relay; 
        private bool networkConnected;
        private string groupId;
        private Control pickerButton; 
        
        private object iLock; 

<<<<<<< HEAD
        public HardMainScreen(HardLightSelection hardLightSelection, StratHub_Base.StratHubBase hub_base, Caller caller, string inLightId, string groupId)
        {
            this.caller = caller; 
            this.hardLightSelection = hardLightSelection; 
=======
        /// <summary>
        /// Main Screen for light control based on 3.2" Raspberry Pi touch screen
        /// </summary>
        /// <param name="hardLightSelection"></param>
        /// <param name="hub_base"></param>
        /// <param name="caller"></param>
        /// <param name="inLightId"></param>
        /// <param name="groupId"></param>
        public HardMainScreen(HardLightSelection hardLightSelection, StratHub_Base.StratHubBase hub_base, Caller caller, string inLightId, string groupId, Control pickerButton=null)
        {
            this.caller = caller;
            this.hardLightSelection = hardLightSelection;
            this.pickerButton = pickerButton; 
>>>>>>> macco
            this.hub_base = hub_base;
            lightId = inLightId;
            this.groupId = groupId; 

            iLock = new object(); 

            InitializeComponent();
            
            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

            lock (iLock)
                networkConnected = this.caller.InternetConnected();

            new Task(CheckInternet).Start();

            if (lightId != null)
            {
                hub_base.UseLight(lightId.ToString()); 
            }
            else if (groupId != null)
            {

            }

            lock (iLock)
            {
                if (networkConnected)
                {
                    status = this.hub_base.GetStatus(inLightId);
                    this.InitValues(status);
                }
            }
        }

        /// <summary>
        /// Iniitalizes values 
        /// </summary>
        /// <param name="status"></param>
        private void InitValues(Dictionary<string, object> status)
        {
            //Sliders 
            var input_brightness = status["brightness"];
            var kelvin = status["kelvin"];

            //Color parts
            var power = status["power"];
            var hue = (float)(status["hue"]);
            var saturation = (float)(status["saturation"]);
            var rgb = ColorFromAhsb(255, hue, saturation, (float)(input_brightness));
            System.Drawing.Color sysColor = Color.FromArgb(rgb.R, rgb.G, rgb.B); 
            btnOnOff.BackColor = sysColor; 

            //Brightness and Temperature 
            double dbrightness = double.Parse(input_brightness.ToString());
            int brightness = Convert.ToInt32(dbrightness * 100.0);
            ChooseProperBGImage(brightness); 
            trackBarBrightness.Value = brightness; 
            trackBarTempChange.Value = int.Parse(kelvin.ToString()); 
        }

        /// <summary>
        /// Changes color from HSB to System Color
        /// </summary>
        /// <param name="a"></param>
        /// <param name="h"></param>
        /// <param name="s"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Color ColorFromAhsb(int a, float h, float s, float b)
        {
            if (0 == s)
            {
                return Color.FromArgb(a, Convert.ToInt32(b * 255),
                  Convert.ToInt32(b * 255), Convert.ToInt32(b * 255));
            }

            float fMax, fMid, fMin;
            int iSextant, iMax, iMid, iMin;

            if (0.5 < b)
            {
                fMax = b - (b * s) + s;
                fMin = b + (b * s) - s;
            }
            else
            {
                fMax = b + (b * s);
                fMin = b - (b * s);
            }

            iSextant = (int)Math.Floor(h / 60f);
            if (300f <= h)
            {
                h -= 360f;
            }
            h /= 60f;
            h -= 2f * (float)Math.Floor(((iSextant + 1f) % 6f) / 2f);
            if (0 == iSextant % 2)
            {
                fMid = h * (fMax - fMin) + fMin;
            }
            else
            {
                fMid = fMin - h * (fMax - fMin);
            }

            iMax = Convert.ToInt32(fMax * 255);
            iMid = Convert.ToInt32(fMid * 255);
            iMin = Convert.ToInt32(fMin * 255);

            switch (iSextant)
            {
                case 1:
                    return Color.FromArgb(a, iMid, iMax, iMin);
                case 2:
                    return Color.FromArgb(a, iMin, iMax, iMid);
                case 3:
                    return Color.FromArgb(a, iMin, iMid, iMax);
                case 4:
                    return Color.FromArgb(a, iMid, iMin, iMax);
                case 5:
                    return Color.FromArgb(a, iMax, iMin, iMid);
                default:
                    return Color.FromArgb(a, iMax, iMid, iMin);
            }
        }

        /// <summary>
        /// Sets control values when network is disconnected
        /// </summary>
        private void OnNetworkDisconnect()
        {
            lblBrightness.Enabled = false;
            lblTemperature.Enabled = false;
            lnkChangeColor.Enabled = false;
        }

        /// <summary>
        /// Sets control values when network is connected
        /// </summary>
        private void OnNetworkConnect()
        {
            lblBrightness.Enabled = true;
            lblTemperature.Enabled = true;
            lnkChangeColor.Enabled = true;
        }

        /// <summary>
        /// Change the brightness of the light. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void brightnessChange_Scroll(object sender, EventArgs e)
        {
            if (!networkConnected) return;
            float brightness = trackBarBrightness.Value / 100.0f;
            this.hub_base.UseLight(lightId);
            this.hub_base.ChangeBrightness(lightId, brightness);
        }

        /// <summary>
        /// Change the temperature of the light. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tempChange_Scroll(object sender, EventArgs e)
        {
            if (!networkConnected) return;
            int kelvin = trackBarTempChange.Value;
            this.hub_base.UseLight(lightId);
            this.hub_base.ChangeTemperature(lightId, kelvin);
        }

        /// <summary>
        /// Toggle the light on and off
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOnOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (networkConnected)
                {
                    this.hub_base.UseLight(lightId);
                    this.hub_base.TogglePower();
                }
                else
                {
                    if (hub_base.IsLightOn())
                        this.caller.StopProcess(); 
                    else
                        this.caller.CallPython("RelayPin.py");
                }
            }
            catch
            {
                if (hub_base.IsLightOn())
                    this.caller.StopProcess();
                else
                    this.caller.CallPython("RelayPin.py");
            }
        }

        /// <summary>
        /// Open the color picker to change the color. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkChangeColor_LinkClicked(object sender, EventArgs e)
        {
            if (!networkConnected) return;
            HardColorPicker picker = new HardColorPicker(this.hub_base, this, lightId); 
            picker.ShowDialog();
        }

        /// <summary>
        /// Change the brightness slider label
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void brightnessChange_ValueChanged(object sender, EventArgs e)
        {
            int brightness = trackBarBrightness.Value;
            ChooseProperBGImage(brightness); 
            lblBrightness.Text = brightness.ToString() + " %";
        }

        /// <summary>
        /// Change the temperature slider label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tempChange_ValueChanged(object sender, EventArgs e)
        {
            lblTemperature.Text = trackBarTempChange.Value.ToString() + " k";
        }

<<<<<<< HEAD
        private void btnLightSelectionScreen_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close(); 
            if (this.btnLightSelectionScreen.Visible)
                this.hardLightSelection.Show(); 
            else
                this.hardLightSelection.ShowDialog();
        }

=======
        /// <summary>
        /// Return to the light selection screen event handler. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLightSelectionScreen_Click(object sender, EventArgs e)
        {
            hardLightSelection.Refresh();
            hardLightSelection.Focus();
            hardLightSelection.Activate();
            hardLightSelection.Show();
            hardLightSelection.BringToFront();
            this.Close(); 
        }

        private void ChooseProperBGImage(int brightness)
        {
            if (brightness >= 80)
            {
                this.btnOnOff.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_100w;
            }
            else if (brightness >= 60)
            {
                this.btnOnOff.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_80w;
            }
            else if (brightness >= 40)
            {
                this.btnOnOff.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_60w;
            }
            else if (brightness >= 20)
            {
                this.btnOnOff.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_40w;
            }
            else if (brightness > 0)
            {
                this.btnOnOff.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_20w;
            }
            else
            {
                this.btnOnOff.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_0w;
            }
        }

        /// <summary>
        /// Changes the background color of the on/off button
        /// </summary>
        /// <param name="color"></param>
>>>>>>> macco
        internal void ChangeButtonColor(Color color)
        {
            btnOnOff.BackColor = color;
            if (pickerButton != null)
                pickerButton.BackColor = color; 
        }

        /// <summary>
        /// Checks for internet connection
        /// </summary>
        private void CheckInternet()
        {
            while (true)
            {
                lock (iLock)
                    networkConnected = this.caller.InternetConnected();

                lock (iLock)
                {
                    if (networkConnected)
                    {
                        InternetConnected();
                    }
                    else
                    {
                        NoInternet();
                    }
                }
                Task.Delay(3000); 
            }
        }

        /// <summary>
        /// Invokes actions if the internet is not connected
        /// </summary>
        private void NoInternet()
        {
            try
            {
                if (trackBarBrightness.InvokeRequired)
                    trackBarBrightness.Invoke(new Action(() => { trackBarBrightness.Enabled = false; }));
                else
                    trackBarBrightness.Enabled = false;
                if (trackBarTempChange.InvokeRequired)
                    trackBarTempChange.Invoke(new Action(() => { trackBarTempChange.Enabled = false; }));
                else
                    trackBarTempChange.Enabled = false;
                if (lnkChangeColor.InvokeRequired)
                    lnkChangeColor.Invoke(new Action(() => { lnkChangeColor.Enabled = false; }));
                else
                    lnkChangeColor.Enabled = false;
            }
            catch { }
        }

        /// <summary>
        /// Invokes actions if the internet is connected
        /// </summary>
        private void InternetConnected()
        {
            try
            {
                if (trackBarBrightness.InvokeRequired)
                    trackBarBrightness.Invoke(new Action(() => { trackBarBrightness.Enabled = true; }));
                else
                    trackBarBrightness.Enabled = true;
                if (trackBarTempChange.InvokeRequired)
                    trackBarTempChange.Invoke(new Action(() => { trackBarTempChange.Enabled = true; }));
                else
                    trackBarTempChange.Enabled = true;
                if (lnkChangeColor.InvokeRequired)
                    lnkChangeColor.Invoke(new Action(() => { lnkChangeColor.Enabled = true; }));
                else
                    lnkChangeColor.Enabled = true; 
            }
            catch { }
        }
<<<<<<< HEAD
        
=======

        /// <summary>
        /// Helps with certificates with older versions of Mono
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
>>>>>>> macco
        public bool MyRemoteCertificateValidationCallback(System.Object sender,
    X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            // If there are errors in the certificate chain,
            // look at each error to determine the cause.
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                for (int i = 0; i < chain.ChainStatus.Length; i++)
                {
                    if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        continue;
                    }
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                        break;
                    }
                }
            }
            return isOk;
        }
    }
}
