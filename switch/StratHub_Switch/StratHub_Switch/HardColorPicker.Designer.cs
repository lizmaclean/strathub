﻿using Cyotek.Windows.Forms;

namespace StratHub_Switch
{
    partial class HardColorPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new StratHub_Switch.RoundButton();
            this.colorSlider = new Cyotek.Windows.Forms.HueColorSlider();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(58, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 40);
            this.button1.TabIndex = 4;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.lblBack_LinkClicked);
            // 
            // colorSlider
            // 
            this.colorSlider.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.colorSlider.Cursor = System.Windows.Forms.Cursors.Cross;
            this.colorSlider.Location = new System.Drawing.Point(12, 46);
            this.colorSlider.Name = "colorSlider";
            this.colorSlider.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.colorSlider.Size = new System.Drawing.Size(187, 269);
            this.colorSlider.TabIndex = 3;
            this.colorSlider.ValueChanged += new System.EventHandler(this.colorSlider_ColorChanged);
            // 
            // HardColorPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(205, 320);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.colorSlider);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HardColorPicker";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion
        private HueColorSlider colorSlider;
        //private System.Windows.Forms.Button button1;
        private RoundButton button1; 
    }
}