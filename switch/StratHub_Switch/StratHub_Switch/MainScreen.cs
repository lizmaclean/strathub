﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StratHub_Base;

namespace StratHub_Switch
{
    public partial class MainScreen : Form
    {
        private StratHub_Base.StratHubBase hub_base;
        private static string light_key = "test_light";

        private Caller caller;
        private bool networkConnected; 

        public MainScreen(StratHub_Base.StratHubBase hub_base)
        {
            this.hub_base = hub_base;
            caller = new Caller();
            networkConnected = caller.InternetConnected(); 
            InitializeComponent();
        }

        private void InitValues(Dictionary<string, object> status)
        {
            //Sliders 
            var input_brightness = status["brightness"];
            var kelvin = status["kelvin"];

            //Color parts
            var power = status["power"];

            double dbrightness = double.Parse(input_brightness.ToString());
            int brightness = Convert.ToInt32(dbrightness * 100.0);

            trackBarBrightness.Value = brightness;
            trackBarTempChange.Value = int.Parse(kelvin.ToString());
        }

        private void OnNetworkDisconnect()
        {
            lblBrightness.Enabled = false;
            lblTemperature.Enabled = false;
            lnkChangeColor.Enabled = false; 
        }

        private void OnNetworkConnect()
        {
            lblBrightness.Enabled = true;
            lblTemperature.Enabled = true;
            lnkChangeColor.Enabled = true;
        }

        private void brightnessChange_Scroll(object sender, EventArgs e)
        {
            if (!networkConnected) return;
            int brightness = trackBarBrightness.Value;
            this.hub_base.UseLight(light_key);

            //Task newTask = new Task(() =>
            //{
            //    this.hub_base.ChangeBrightness(light_key, brightness);
            //});
            //newTask.Start();
            this.hub_base.ChangeBrightness(light_key, brightness);
        }

        private void tempChange_Scroll(object sender, EventArgs e)
        {
            if (!networkConnected) return; 
            int kelvin = trackBarTempChange.Value;
            this.hub_base.UseLight(light_key);
            this.hub_base.ChangeTemperature(light_key, kelvin);
        }

        private void btnOnOff_Click(object sender, EventArgs e)
        {
            this.hub_base.UseLight(light_key);
            this.hub_base.TogglePower();
        }

        private void lnkChangeColor_LinkClicked(object sender, EventArgs e)
        {
            if (!networkConnected) return;
            ColorPicker picker = new ColorPicker(this.hub_base, this);
            picker.ShowDialog();
        }

        private void brightnessChange_ValueChanged(object sender, EventArgs e)
        {
            lblBrightness.Text = trackBarBrightness.Value.ToString() + " %";
        }

        private void tempChange_ValueChanged(object sender, EventArgs e)
        {
            lblTemperature.Text = trackBarTempChange.Value.ToString() + " k";
        }

        internal void ChangeButtonColor(Color color)
        {
            btnOnOff.ForeColor = color;
            btnOnOff.BackColor = color;
        }
    }
}
