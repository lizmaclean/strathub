﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Net;

namespace StratHub_Switch
{
    public class Caller
    {
        private Process process;
        private ProcessStartInfo startInfo; 

        /// <summary>
        /// Caller class deals with internet checking and raspberry pi relay checks. 
        /// </summary>
        public Caller()
        {
            process = new Process();
            startInfo = new ProcessStartInfo(); 
        }

        /// <summary>
        /// Checks if the internet is connected. 
        /// </summary>
        /// <returns></returns>
        public bool InternetConnected()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Calls python file and runs the code. 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public Process CallPython(string filePath)
        {
            try
            {
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.FileName = "/bin/bash";
                startInfo.Arguments = "-c \"python " + filePath + "\"";
                startInfo.UseShellExecute = true;
                startInfo.RedirectStandardOutput = false;
                startInfo.RedirectStandardError = false;
                process.StartInfo = startInfo;
                process.Start();
                return process;
            }
            catch(Exception) { return null; }
        }

        /// <summary>
        /// Stops the python process currently running from the caller instance. 
        /// </summary>
        public void StopProcess()
        {
            process.Kill(); 
        }
    }
}
