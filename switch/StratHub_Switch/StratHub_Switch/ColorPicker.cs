﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cyotek.Windows.Forms;
using StratHub_Base; 

namespace StratHub_Switch
{
    public partial class ColorPicker : BaseForm
    {
        private StratHub_Base.StratHubBase hub_base;
        private static string light_key = "d073d52360de";

        private MainScreen mainScreen; 

        public ColorPicker(StratHub_Base.StratHubBase hub_base, MainScreen mainScreen)
        {
            this.hub_base = hub_base;
            this.mainScreen = mainScreen; 
            InitializeComponent();
        }
        
        /// <summary>
        /// Event handler on what to do when the color is changes. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void colorWheel_ColorChanged(object sender, EventArgs e)
        {
            panel1.BackColor = colorWheel.Color;

            SetButtonColor();

            hub_base.ChangeColor(light_key, (int)colorWheel.Color.GetHue(), colorWheel.Color.GetSaturation(), 5000); 
            //hub_base.ChangeColor(light_key, colorWheel.Color.R, colorWheel.Color.G, colorWheel.Color.B, 5000);
        }

        internal void SetButtonColor()
        {
            mainScreen.ChangeButtonColor(colorWheel.Color); 
        }

        private void lblBack_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            mainScreen.Visible = true;
            this.Close();
        }
    }
}
