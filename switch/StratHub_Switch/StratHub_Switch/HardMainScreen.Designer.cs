﻿namespace StratHub_Switch
{
    partial class HardMainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBrightness = new System.Windows.Forms.Label();
            this.lblTemperature = new System.Windows.Forms.Label();
            this.trackBarBrightness = new System.Windows.Forms.TrackBar();
            this.trackBarTempChange = new System.Windows.Forms.TrackBar();
            this.btnOnOff = new System.Windows.Forms.PictureBox();
            this.lnkChangeColor = new System.Windows.Forms.PictureBox();
            this.btnLightSelectionScreen = new StratHub_Switch.RoundButton();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBrightness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTempChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOnOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lnkChangeColor)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBrightness
            // 
            this.lblBrightness.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBrightness.AutoSize = true;
            this.lblBrightness.Location = new System.Drawing.Point(145, 293);
            this.lblBrightness.Name = "lblBrightness";
            this.lblBrightness.Size = new System.Drawing.Size(24, 13);
            this.lblBrightness.TabIndex = 5;
            this.lblBrightness.Text = "0 %";
            this.lblBrightness.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTemperature
            // 
            this.lblTemperature.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTemperature.AutoSize = true;
            this.lblTemperature.Location = new System.Drawing.Point(30, 293);
            this.lblTemperature.Name = "lblTemperature";
            this.lblTemperature.Size = new System.Drawing.Size(40, 13);
            this.lblTemperature.TabIndex = 6;
            this.lblTemperature.Text = "2500 k";
            this.lblTemperature.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBarBrightness
            // 
            this.trackBarBrightness.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.trackBarBrightness.Dock = System.Windows.Forms.DockStyle.Right;
            this.trackBarBrightness.Location = new System.Drawing.Point(160, 0);
            this.trackBarBrightness.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.trackBarBrightness.Maximum = 100;
            this.trackBarBrightness.Name = "trackBarBrightness";
            this.trackBarBrightness.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarBrightness.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.trackBarBrightness.RightToLeftLayout = true;
            this.trackBarBrightness.Size = new System.Drawing.Size(45, 320);
            this.trackBarBrightness.TabIndex = 7;
            this.trackBarBrightness.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarBrightness.ValueChanged += new System.EventHandler(this.brightnessChange_ValueChanged);
            this.trackBarBrightness.MouseCaptureChanged += new System.EventHandler(this.brightnessChange_Scroll);
            // 
            // trackBarTempChange
            // 
            this.trackBarTempChange.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.trackBarTempChange.Dock = System.Windows.Forms.DockStyle.Left;
            this.trackBarTempChange.Location = new System.Drawing.Point(0, 0);
            this.trackBarTempChange.Maximum = 9000;
            this.trackBarTempChange.Minimum = 2500;
            this.trackBarTempChange.Name = "trackBarTempChange";
            this.trackBarTempChange.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarTempChange.Size = new System.Drawing.Size(45, 320);
            this.trackBarTempChange.TabIndex = 8;
            this.trackBarTempChange.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarTempChange.Value = 2500;
            this.trackBarTempChange.ValueChanged += new System.EventHandler(this.tempChange_ValueChanged);
            this.trackBarTempChange.MouseCaptureChanged += new System.EventHandler(this.tempChange_Scroll);
            // 
            // btnOnOff
            // 
            this.btnOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnOnOff.BackgroundImage = global::StratHub_Switch.Properties.Resources.real_bulb_0w;
            this.btnOnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
<<<<<<< HEAD
            this.btnOnOff.Location = new System.Drawing.Point(33, 49);
            this.btnOnOff.Name = "btnOnOff";
            this.btnOnOff.Size = new System.Drawing.Size(136, 206);
=======
            this.btnOnOff.Location = new System.Drawing.Point(40, 50);
            this.btnOnOff.Name = "btnOnOff";
            this.btnOnOff.Size = new System.Drawing.Size(120, 190);
>>>>>>> macco
            this.btnOnOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnOnOff.TabIndex = 10;
            this.btnOnOff.TabStop = false;
            this.btnOnOff.Click += new System.EventHandler(this.btnOnOff_Click);
            // 
            // lnkChangeColor
            // 
            this.lnkChangeColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lnkChangeColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lnkChangeColor.Image = global::StratHub_Switch.Properties.Resources._382px_BYR_color_wheel;
            this.lnkChangeColor.Location = new System.Drawing.Point(80, 261);
            this.lnkChangeColor.Name = "lnkChangeColor";
            this.lnkChangeColor.Size = new System.Drawing.Size(42, 45);
            this.lnkChangeColor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lnkChangeColor.TabIndex = 7;
            this.lnkChangeColor.TabStop = false;
            this.lnkChangeColor.Click += new System.EventHandler(this.lnkChangeColor_LinkClicked);
            // 
            // btnLightSelectionScreen
            // 
            this.btnLightSelectionScreen.Location = new System.Drawing.Point(50, 5);
            this.btnLightSelectionScreen.Name = "btnLightSelectionScreen";
            this.btnLightSelectionScreen.Size = new System.Drawing.Size(104, 42);
            this.btnLightSelectionScreen.TabIndex = 11;
            this.btnLightSelectionScreen.Text = "Light Selection";
            this.btnLightSelectionScreen.UseVisualStyleBackColor = true;
            this.btnLightSelectionScreen.Click += new System.EventHandler(this.btnLightSelectionScreen_Click);
            // 
            // HardMainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(205, 320);
            this.Controls.Add(this.btnLightSelectionScreen);
            this.Controls.Add(this.lblBrightness);
            this.Controls.Add(this.lblTemperature);
            this.Controls.Add(this.btnOnOff);
            this.Controls.Add(this.lnkChangeColor);
            this.Controls.Add(this.trackBarBrightness);
            this.Controls.Add(this.trackBarTempChange);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HardMainScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainScreen";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBrightness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTempChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOnOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lnkChangeColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblBrightness;
        private System.Windows.Forms.Label lblTemperature;
        private System.Windows.Forms.TrackBar trackBarBrightness;
        private System.Windows.Forms.TrackBar trackBarTempChange;
        private System.Windows.Forms.PictureBox btnOnOff;
        private System.Windows.Forms.PictureBox lnkChangeColor;
        //private System.Windows.Forms.Button btnLightSelectionScreen;
        private RoundButton btnLightSelectionScreen; 
    }
}