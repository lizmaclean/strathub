import sys
import time
import gpiozero

RELAY_PIN=21

relay = gpiozero.OutputDevice(RELAY_PIN, active_high=False, initial_value=False)

def set_relay(status):
    print("Setting Relay")
    if status:
        relay.on()
    else: 
        relay.off()

def toggle_relay():
    print("Toggling Relay")
    relay.toggle()

def main_loop():
    set_relay(False)

    toggle_relay()
    while 1:
        time.sleep(1)

if __name__ == "__main__":
    try: 
        main_loop()
    except Exception as ex: 
        set_relay(False)
        print ex
        print("Exiting")
        sys.exit(0)
