// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace StratHub
{
    [Register ("ViewController2")]
    partial class ViewController2
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton bulbOne { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton bulbTwo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelOne { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelTwo { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (bulbOne != null) {
                bulbOne.Dispose ();
                bulbOne = null;
            }

            if (bulbTwo != null) {
                bulbTwo.Dispose ();
                bulbTwo = null;
            }

            if (labelOne != null) {
                labelOne.Dispose ();
                labelOne = null;
            }

            if (labelTwo != null) {
                labelTwo.Dispose ();
                labelTwo = null;
            }
        }
    }
}