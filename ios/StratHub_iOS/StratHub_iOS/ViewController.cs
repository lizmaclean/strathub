﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using static System.NMath;
using AdvancedColorPicker;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using StratHub_Base;

namespace StratHub
{
    public partial class ViewController : UIViewController
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public ViewController()
        {
        }

        static string token = "c4ee25616303ac1c55af8037fafe08ed03064813fc0acc0d3d9a01683df18f3c";
        static string response = "";
        public static string bulbID { get; set; }
        static Dictionary<string, object> status = new Dictionary<string, object>();
        int R;
        int G;
        int B;
        int isHidden = 0;
        private StratHub_Base.StratHub_Base base_api = StratHub_Base.StratHub_Base.GetInstance(token);

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Console.WriteLine("bulb: " +  bulbID);
            base_api.UseLight(bulbID);
            Dictionary<string, object> status = base_api.GetStatus();
            foreach (object k in status) {
                Console.WriteLine(k);
            }




            //base_api.DiscoverLights();
            //lights = base_api.GetAllLANLights();
            //Console.WriteLine("before");
            //base_api.UseLight(lights[0]);
            //Console.WriteLine(lights[0]);
            //Console.WriteLine("after");
            //Dictionary<string, object> status = base_api.GetStatus(null);
            HsvToRgb(Convert.ToDouble(status["hue"]), Convert.ToDouble(status["saturation"]), Convert.ToDouble(status["brightness"]), out R, out G, out B);
            UIColor currentColor = new UIColor(red: R, green: G, blue: B, alpha: 1);
            switchToggle.SetTitleColor(UIColor.Black, UIControlState.Normal);
            switchToggle.SetImage(UIImage.FromBundle("BulbIcon"), UIControlState.Normal);
            switchToggle.TouchUpInside += (object sender, EventArgs e) =>
            {
                Console.WriteLine(base_api.IsLightOn(bulbID));
                base_api.UseLight(bulbID);
                response = base_api.TogglePower(bulbID);
                string ret_val = string.Format("Http Response: {0}", response);
                Console.WriteLine(ret_val);
                Console.WriteLine(base_api.IsLightOn(bulbID));
                //string ret_val = string.Format("Http Response: {0}", response);
                //Console.WriteLine(ret_val);
 
            };


            kelvinSlider.MinValue = 1500;
            kelvinSlider.MaxValue = 9000;
            kelvinSlider.Value = Convert.ToSingle(status["kelvin"]);
            currentTemp.Text = kelvinSlider.Value.ToString() + "K";
            kelvinSlider.MinimumTrackTintColor = UIColor.Blue;
            kelvinSlider.MaximumTrackTintColor = UIColor.Orange;
            kelvinSlider.Transform = CGAffineTransform.MakeRotation((float)PI/ 2);
            float stepValue = 1.0f;
            kelvinSlider.Continuous = true;
            kelvinSlider.ValueChanged += (object sender, EventArgs e) =>
            {
                float nextStep = (float)Math.Round(kelvinSlider.Value / stepValue);
                kelvinSlider.Value = (nextStep*stepValue);
                currentTemp.Text = kelvinSlider.Value.ToString() + "K";
                //SetTemp(light_key, (int)kelvinSlider.Value);
              //  kelvinTemp.Text = "Kelvin: " + kelvinSlider.Value.ToString();
            };
            kelvinSlider.TouchUpInside += (object sender, EventArgs e) =>
            {
                //Console.WriteLine((int)kelvinSlider.Value);
                base_api.UseLight(bulbID);
                base_api.ChangeTemperature(bulbID, (int)kelvinSlider.Value);

            };

            tempSetSlider.Hidden = true;
            tempSetSlider.MinValue = 1500;
            tempSetSlider.MaxValue = 9000;
            tempSetSlider.Value = kelvinSlider.Value;
            currentTemp.Text = kelvinSlider.Value.ToString() + "K";
            tempSetSlider.MinimumTrackTintColor = UIColor.Blue;
            tempSetSlider.MaximumTrackTintColor = UIColor.Orange;
            tempSetSlider.Transform = CGAffineTransform.MakeRotation((float)PI / 2);
            float setStepValue = 500.0f;
            tempSetSlider.Continuous = true;
            tempSetSlider.ValueChanged += (object sender, EventArgs e) =>
            {
                float nextStep = (float)Math.Round(tempSetSlider.Value / setStepValue);
                tempSetSlider.Value = (nextStep * setStepValue);
                currentTemp.Text = tempSetSlider.Value.ToString() + "K";
                //SetTemp(light_key, (int)kelvinSlider.Value);
                //  kelvinTemp.Text = "Kelvin: " + kelvinSlider.Value.ToString();
            };
            tempSetSlider.TouchUpInside += (object sender, EventArgs e) =>
            {
                //Console.WriteLine((int)kelvinSlider.Value);
                base_api.UseLight(bulbID);
                base_api.ChangeTemperature(bulbID, (int)tempSetSlider.Value);

            };

            brightnessSlider.MinValue = 0;
            brightnessSlider.MaxValue = 100;
            brightnessSlider.Value = ((float)Math.Round((Convert.ToSingle(status["brightness"])) * 100));
            currentBright.Text = brightnessSlider.Value.ToString() + "%";
            brightnessSlider.Transform = CGAffineTransform.MakeRotation(((float)PI*3) / 2);
            float brightnessStepValue = 1.0f;
            brightnessSlider.Continuous = true;
            brightnessSlider.ValueChanged += (object sender, EventArgs e) =>
            {
                float brightnessNextStep = (float)Math.Round(brightnessSlider.Value / brightnessStepValue);
                brightnessSlider.Value = (brightnessNextStep * brightnessStepValue);
                currentBright.Text = brightnessSlider.Value.ToString() + "%";
                //base_api.ChangeBrightness(bulbID, (int)brightnessSlider.Value);
                //brightnessValue.Text = "Brightness: " + brightnessSlider.Value.ToString();
            };
            brightnessSlider.TouchUpInside += (object sender, EventArgs e) =>
            {
                //Console.WriteLine((int)brightnessSlider.Value);
                base_api.UseLight(bulbID);
                base_api.ChangeBrightness(bulbID, ((float)brightnessSlider.Value / 100));
            };



            setScroll.SetTitleColor(UIColor.Black, UIControlState.Normal);
            setScroll.TouchUpInside += (object sender, EventArgs e) =>
            {
                if (isHidden == 0) {
                    tempSetSlider.Hidden = false;
                    kelvinSlider.Hidden = true;
                    isHidden = 1;
                }
                else
                {
                    tempSetSlider.Hidden = true;
                    kelvinSlider.Value = tempSetSlider.Value;
                    kelvinSlider.Hidden = false;
                    isHidden = 0;
                }
            };


            colorPicker.TouchUpInside += async delegate 
            {
                status = base_api.GetStatus();
                HsvToRgb(Convert.ToDouble(status["hue"]), Convert.ToDouble(status["saturation"]), Convert.ToDouble(status["brightness"]), out R, out G, out B);
                currentColor = new UIColor(red: R, green: G, blue: B, alpha: 1);
                var color = await ColorPickerViewController.PresentAsync(UIApplication.SharedApplication.KeyWindow.RootViewController.PresentedViewController,"Pick a color!", currentColor);

                // changethe background
                nfloat red, green, blue, alpha;
                color.GetRGBA(out red, out green, out blue, out alpha);
                red = (red * 255);
                green = (green * 255);
                blue = (blue * 255);
                Console.WriteLine("Color: {0} {1} {2}", (int)red, (int)green, (int)blue);
                base_api.UseLight(bulbID);
                base_api.ChangeColor(bulbID, (int)red, (int)green, (int)blue, -1);
            };
        }

        void HsvToRgb(double h, double S, double V, out int r, out int g, out int b)
        {
            double H = h;
            while (H < 0) { H += 360; };
            while (H >= 360) { H -= 360; };
            double R, G, B;
            if (V <= 0)
            { R = G = B = 0; }
            else if (S <= 0)
            {
                R = G = B = V;
            }
            else
            {
                double hf = H / 60.0;
                int i = (int)Math.Floor(hf);
                double f = hf - i;
                double pv = V * (1 - S);
                double qv = V * (1 - S * f);
                double tv = V * (1 - S * (1 - f));
                switch (i)
                {

                    // Red is the dominant color

                    case 0:
                        R = V;
                        G = tv;
                        B = pv;
                        break;

                    // Green is the dominant color

                    case 1:
                        R = qv;
                        G = V;
                        B = pv;
                        break;
                    case 2:
                        R = pv;
                        G = V;
                        B = tv;
                        break;

                    // Blue is the dominant color

                    case 3:
                        R = pv;
                        G = qv;
                        B = V;
                        break;
                    case 4:
                        R = tv;
                        G = pv;
                        B = V;
                        break;

                    // Red is the dominant color

                    case 5:
                        R = V;
                        G = pv;
                        B = qv;
                        break;

                    // Just in case we overshoot on our math by a little, we put these here. Since its a switch it won't slow us down at all to put these here.

                    case 6:
                        R = V;
                        G = tv;
                        B = pv;
                        break;
                    case -1:
                        R = V;
                        G = pv;
                        B = qv;
                        break;

                    // The color is not defined, we should throw an error.

                    default:
                        //LFATAL("i Value error in Pixel conversion, Value is %d", i);
                        R = G = B = V; // Just pretend its black/white
                        break;
                }
            }
            r = Clamp((int)(R * 255.0));
            g = Clamp((int)(G * 255.0));
            b = Clamp((int)(B * 255.0));
        }

        /// <summary>
        /// Clamp a value to 0-255
        /// </summary>
        int Clamp(int i)
        {
            if (i < 0) return 0;
            if (i > 255) return 255;
            return i;
        }

       /* public async void SetBright(string key, int value) {

            base_api.ChangeBrightness(key, value);
        } */

      /*  public async Task<string> SetTemp(string key, int value){
            base_api.ChangeTemperate(key, value);
        } */

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
