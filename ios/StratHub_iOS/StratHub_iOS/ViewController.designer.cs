// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace StratHub
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISlider brightnessSlider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton colorPicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel currentBright { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel currentTemp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISlider kelvinSlider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton setScroll { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton switchToggle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISlider tempSetSlider { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (brightnessSlider != null) {
                brightnessSlider.Dispose ();
                brightnessSlider = null;
            }

            if (colorPicker != null) {
                colorPicker.Dispose ();
                colorPicker = null;
            }

            if (currentBright != null) {
                currentBright.Dispose ();
                currentBright = null;
            }

            if (currentTemp != null) {
                currentTemp.Dispose ();
                currentTemp = null;
            }

            if (kelvinSlider != null) {
                kelvinSlider.Dispose ();
                kelvinSlider = null;
            }

            if (setScroll != null) {
                setScroll.Dispose ();
                setScroll = null;
            }

            if (switchToggle != null) {
                switchToggle.Dispose ();
                switchToggle = null;
            }

            if (tempSetSlider != null) {
                tempSetSlider.Dispose ();
                tempSetSlider = null;
            }
        }
    }
}