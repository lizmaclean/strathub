using System;
using UIKit;
using CoreGraphics;
using Foundation;
using static System.NMath;
using AdvancedColorPicker;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using StratHub_Base;

namespace StratHub
{
    public partial class ViewController2 : UIViewController
    {
        public ViewController2 (IntPtr handle) : base (handle)
        {
        }
        //static string token = "c4ee25616303ac1c55af8037fafe08ed03064813fc0acc0d3d9a01683df18f3c";
        //List<String> lights;
        //private static StratHub_Base.StratHub_Base base_api = StratHub_Base.StratHub_Base.GetInstance(token);

        public override void ViewDidLoad()
        {
           // lights = base_api.GetAllHTTPLights();
            //labelOne.Hidden = true;
            //labelTwo.Hidden = true;
            //bulbOne.Hidden = true;
           // bulbTwo.Hidden = true;
           // if (lights[0] != null)
            //{
            //    labelOne.Hidden = false;
           //     bulbOne.Hidden = false;
            labelOne.Text = "d073d523ec89";
           // }
           // if (lights[1] != null)
           // {
           //     labelTwo.Hidden = false;
           //     bulbTwo.Hidden = false;
            labelTwo.Text = "d073d52360de";
           // }
            ViewController.bulbID = null;

            bulbOne.TouchDown += (object sender, EventArgs e) =>
            {
                ViewController.bulbID = "d073d523ec89";
                Console.WriteLine("down bulb: " + ViewController.bulbID);
            };

            bulbOne.TouchUpInside += (object sender, EventArgs e) =>
            {
                ViewController.bulbID = "d073d523ec89";
                Console.WriteLine("bulb: " + ViewController.bulbID);
            };

            bulbTwo.TouchDown += (object sender, EventArgs e) =>
            {
                ViewController.bulbID = "d073d52360de";
                Console.WriteLine("down bulb: " + ViewController.bulbID);
            };

            bulbTwo.TouchUpInside += (object sender, EventArgs e) =>
            {
                ViewController.bulbID = "d073d52360de";
                Console.WriteLine("bulb: " + ViewController.bulbID);
            };
        }
    }
}