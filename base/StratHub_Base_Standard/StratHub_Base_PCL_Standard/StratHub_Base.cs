﻿using System;
using System.Net.Http;
using StratHub_Base.Data_Models;
using StratHub_Base.IO_Models.ConnectionTypes;
using System.Collections.Generic;
using StratHub_Base.LAN;
using Color = StratHub_Base.Data_Models.Color;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;

namespace StratHub_Base
{
    // API CLASS
    public class StratHub_Base
    {
        private static StratHub_Base _instance = null; 

        private string base_url;
        private LifxClient lifxClient;
        private Account account;
        private Light light;
        private HTTPSConnection connection;

        private bool foundLanLight;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.StratHub_Base"/> class.
        /// </summary>
        private StratHub_Base(string account_token)
        {
            account = null;
            light = null;
            base_url = "https://api.lifx.com/v1/lights/id:{0}";
            connection = new HTTPSConnection();
            account = Account.GetOrMakeAccount(account_token);
            DiscoverLights(); 
            foundLanLight = false;
            try
            {
                if (lifxClient == null)
                {
                    Task.Factory.StartNew(() =>
                    {
                        InitLifxClient().GetAwaiter().GetResult();
                    }).Wait();
                }
            }
            catch { }

            int i = 0;
            while (!foundLanLight || i >= 5)
            {
                Task.Delay(1000).Wait();
                i++;
                if (foundLanLight || i >= 5)
                    break;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.StratHub_Base"/> class.
        /// </summary>
        /// <param name="base_url">Base URL.</param>
        private StratHub_Base(string account_token, string base_url)
        {
            account = Account.GetOrMakeAccount(account_token);
            this.base_url = base_url;
            connection = new HTTPSConnection();
            DiscoverLights(); 
            foundLanLight = false;
            try
            {
                if (lifxClient == null)
                {
                    Task.Factory.StartNew(() =>
                    {
                        InitLifxClient().GetAwaiter().GetResult();
                    }).Wait();
                }
            }
            catch { }

            int i = 0;
            while (!foundLanLight || i >= 5)
            {
                Task.Delay(1000).Wait();
                i++;
                if (foundLanLight || i >= 5)
                    break;
            }
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <returns>The instance.</returns>
        /// <param name="token">Token.</param>
        /// <param name="base_url">Base URL.</param>
        public static StratHub_Base GetInstance(string token, string base_url = null)
        {
            if (_instance == null)
            {
                if (base_url != null)
                {
                    _instance = new StratHub_Base(token, base_url);
                }
                else
                {
                    _instance = new StratHub_Base(token);
                }
            }
            return _instance;
        }

        /// <summary>
        /// Releases all resource used by the <see cref="T:StratHub_Base.StratHub_Base"/> object.
        /// </summary>
        /// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="T:StratHub_Base.StratHub_Base"/>. The
        /// <see cref="Dispose"/> method leaves the <see cref="T:StratHub_Base.StratHub_Base"/> in an unusable state.
        /// After calling <see cref="Dispose"/>, you must release all references to the
        /// <see cref="T:StratHub_Base.StratHub_Base"/> so the garbage collector can reclaim the memory that the
        /// <see cref="T:StratHub_Base.StratHub_Base"/> was occupying.</remarks>
        public void Dispose()
        {
            account.RemoveAllLANLightsFromAccount();
            account.RemoveAllHTTPLightsFromAccount();
            account.RemoveAllHomeGroups();
            lifxClient.StopDeviceDiscovery(); 
            lifxClient.Dispose();
            base_url = null;
            lifxClient = null;
            account = null;
            light = null;
            connection = null;
            foundLanLight = false;
        }

        /// <summary>
        /// Discovers the lights.
        /// </summary>
        private void DiscoverLights()
        {
            var list_lights_url = "https://api.lifx.com/v1/lights/all";
            HttpClient client = connection.GetOrSetupClient(list_lights_url, account.GetToken());
            HttpResponseMessage message = connection.HttpRequest(client, HttpMethod.Get, list_lights_url).Result;
            string response = connection.ParseHTTPResponse(message).Result.ToString();
            connection.Close(connection);
            List<Status> statuses = account.ListLightsOnAccount(connection, list_lights_url).Result;
            foreach (var status in statuses)
            {
                AddHTTPLight(status.id, status.label, status);
            }
            connection.Close(connection);
        }

        /// <summary>
        /// Inits the lifx client.
        /// </summary>
        /// <returns>The lifx client.</returns>
        private async Task<String> InitLifxClient()
        {
            lifxClient = await LifxClient.CreateAsync();
            lifxClient.DeviceDiscovered += DeviceDiscovered;
            //client.DeviceLost += Client_DeviceLost;
            lifxClient.StartDeviceDiscovery();
            return "Done Init Lifx Client";
        }

        /// <summary>
        /// Devices the discovered.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void DeviceDiscovered(object sender, LifxClient.DeviceDiscoveryEventArgs e)
        {
            var bulb = e.Device as LightBulb;
            var label_data = lifxClient.GetDeviceLabelAsync(bulb);
            //light = bulb; 
            //GetStatus(); 
            //HOW DO WE GET THIS WORK WHERE IT DOES IT BY ITSELF?!
            var bulb_id = "d073d52360de"; 
            AddLANLight(bulb_id, bulb);
            foundLanLight = true;
        }

        /// <summary>
        /// Devices the lost.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void DeviceLost(object sender, LifxClient.DeviceDiscoveryEventArgs e)
        {

        }

        /// <summary>
        /// Adds the LAN Light.
        /// </summary>
        /// <param name="light_id">Light identifier.</param>
        /// <param name="light">Light.</param>
        /// <param name="group_name">Group name.</param>
        private void AddLANLight(string light_id, LAN.LightBulb light, string group_name = null)
        {
            light.id = light_id;
            light.key = light_id;
            light.account_token = account.GetToken();
            account.AddLANLight(light_id, light);
            if (!String.IsNullOrEmpty(group_name))
            {
                account.AddHomeGroup(group_name);
            }
        }

        /// <summary>
        /// Adds the lan light.
        /// </summary>
        /// <param name="light_id">Light identifier.</param>
        /// <param name="light_key">Light key.</param>
        /// <param name="group_name">Home Group.</param>
        private void AddHTTPLight(string light_id, string light_key, Status status, string group_name = null)
        {
            light = new Light();
            light.id = light_id;
            light.key = light_key;
            light.status = status; 
            light.account_token = account.GetToken();
            account.AddHTTPLight(light_id, light);
            if (!String.IsNullOrEmpty(group_name))
            {
                account.AddHomeGroup(group_name);
            }
        }

        /// <summary>
        /// Sets the light to use
        /// </summary>
        /// <param name="lightId"></param>
        public void UseLight(string lightId)
        {
            light = account.GetSpecificLight(lightId);
        }

        /// <summary>
        /// Returns true if light it on, false if light is off
        /// </summary>
        /// <param name="lightId"></param>
        /// <returns></returns>
        public bool IsLightOn(string lightId=null)
        {
            if (lightId != null)
            {
                UseLight(lightId);
            }
            if (light.status == null)
            {
                return false; 
            }
            else if (light.status.power == "on")
            {
                return true;
            }
            else
            {
                return false; 
            }
        }

        /// <summary>
        /// Gets all the lan light ids for selection
        /// </summary>
        /// <returns></returns>
        public List<String> GetAllLANLights()
        {
            var lightIds = new List<String>();
            var lights = this.account.GetAllLANLights();
            foreach (var kvLight in lights)
            {
                lightIds.Add(kvLight.Key);
            }
            return lightIds;
        }

        /// <summary>
        /// Gets all the http light ids for selection
        /// </summary>
        /// <returns></returns>
        public List<String> GetAllHTTPLights()
        {
            var lightIds = new List<String>();
            var lights = this.account.GetAllHTTPLights();
            foreach (var kvLight in lights)
            {
                lightIds.Add(kvLight.Key);
            }
            return lightIds;
        }


        //TODO: This can be done better....all the LIFX LAN protocol stuff can, running out of time 
        //Want to try and figure make this apart of the API with classes, not just wrapper
        //also we want to have it resolve the blub by itself like it had before rather than fail over 
        //method. This also applies to all methods that look like this 

        /// <summary>
        /// Gets the status. With keys: power, hue, brightness, kelvin, saturation
        /// </summary>
        /// <returns>The status dictionary. With keys: power, hue, brightness, kelvin, saturation</returns>
        /// <param name="lightId">Light identifier.</param>
        /// <param name="light">Light.</param>
        public Dictionary<string, object> GetStatus(string lightId = null)
        {
            if (lightId != null)
                light = account.GetSpecificLight(lightId);
            try
            {
                var lifxLight = light as LightBulb;
                var state = lifxClient.GetLightStateAsync(lifxLight);
                var status_obj = state.Result;
                Dictionary<string, object> status = new Dictionary<string, object>();
                EStatus onOff = status_obj.IsOn ? EStatus.ON : EStatus.OFF;
                status.Add("power", onOff);
                status.Add("hue", status_obj.Hue);
                status.Add("saturation", status_obj.Saturation);
                status.Add("brightness", status_obj.Brightness);
                status.Add("kelvin", status_obj.Kelvin);
                status.Add("label", status_obj.Label);
                return status;
            }
            catch (Exception)
            {
                EStatus eStatus = light.GetStatus(connection, base_url, light.id).Result;
                Status status_obj = light.status;
                Dictionary<string, object> status = new Dictionary<string, object>();
                connection.Close(connection);
                status.Add("power", eStatus);
                status.Add("hue", status_obj.color.hue);
                status.Add("saturation", status_obj.color.saturation);
                status.Add("brightness", status_obj.brightness);
                status.Add("kelvin", status_obj.color.kelvin); 
                return status;
            }
        }

        /// <summary>
        /// Toggles the power.
        /// </summary>
        /// <returns>The power.</returns>
        /// <param name="lightId">Light key.</param>
        public string TogglePower(string lightId = null)
        {
            Dictionary<string, object> status = GetStatus(lightId);
            if (lightId != null)
                light = account.GetSpecificLight(lightId);
            try
            {
                var lifxLight = light as LightBulb;
                if (status["power"] is EStatus.ON)
                {
                    lifxClient.TurnDeviceOffAsync(lifxLight).GetAwaiter().GetResult();
                }
                else
                {
                    lifxClient.TurnDeviceOnAsync(lifxLight).GetAwaiter().GetResult();
                }
                return "Used Lifx Client";
            }
            catch (Exception)
            {
                string toggle_url = connection.SetupUrl(base_url, light.id, "toggle");
                HttpClient client = connection.GetOrSetupClient(toggle_url, account.GetToken());
                HttpResponseMessage message = connection.HttpRequest(client, HttpMethod.Post, toggle_url).Result;
                string response = connection.ParseHTTPResponse(message).Result.ToString();
                connection.Close(connection);
                return response;
            }
        }

        //TODO: Check this method and see if you can simplify and make more generic 
        /// <summary>
        /// Changes the color of the LIFX light.
        /// </summary>
        /// <returns>The color.</returns>
        /// <param name="lightId">Light key.</param>
        /// <param name="red">Red [0-255].</param>
        /// <param name="green">Green[0-255].</param>
        /// <param name="blue">Blue [0-255].</param>
        /// <param name="kelvin">Kelvin [2500-9000]</param>
        public string ChangeColor(string lightId, int red, int green, int blue, int kelvin = -1)
        {
            Dictionary<string, object> status = GetStatus(lightId);
            if (lightId != null)
                light = account.GetSpecificLight(lightId);
            try
            {
                LAN.Color color = new LAN.Color();
                color.R = (byte)red;
                color.B = (byte)blue;
                color.G = (byte)green;
                ushort[] hsl = LAN.Utilities.RgbToHsl(color);
                var hue = hsl[0];
                var saturation = hsl[1];
                var brightness = hsl[2];
                UInt16 intKelvinUse;
                if (kelvin == -1)
                {
                    intKelvinUse = Convert.ToUInt16(int.Parse(status["kelvin"].ToString()));
                }
                else
                {
                    intKelvinUse = Convert.ToUInt16(kelvin);
                }
                var lifxLight = light as LightBulb;
                lifxClient.SetColorAsync(lifxLight, color, intKelvinUse).GetAwaiter().GetResult();
                return "Used Lifx Client";
            }
            catch (Exception ex)
            {
                UInt16 intKelvinUse;
                if (kelvin == -1)
                {
                    intKelvinUse = Convert.ToUInt16(int.Parse(status["kelvin"].ToString()));
                }
                else
                {
                    intKelvinUse = Convert.ToUInt16(kelvin);
                }

                Dictionary<string, Object> colors = new Dictionary<string, Object>();
                Color newColor = new Color();
                newColor.CheckRGBValue(red, green, blue); 
                newColor.red = red;
                newColor.green = green;
                newColor.blue = blue; 
                //newColor.brightness = (float)brightness; 
                newColor.kelvin = intKelvinUse;
                colors.Add("color", newColor);
                var json = light.BuildColorJSONParams(colors).ToString();
                string toggle_url = connection.SetupUrl(base_url, light.id, "state");
                HttpClient client = connection.GetOrSetupClient(toggle_url, account.GetToken());
                HttpResponseMessage message = connection.HttpRequest(client, HttpMethod.Put, toggle_url, json).Result;
                string response = connection.ParseHTTPResponse(message).Result.ToString();
                connection.Close(connection);
                return response;
            }
        }

        /// <summary>
        /// Changes the brightness.
        /// </summary>
        /// <returns>The brightness.</returns>
        /// <param name="lightId">Light key.</param>
        /// <param name="brightness">Brightness [0.0-1.0]</param>
        public string ChangeBrightness(string lightId, float brightness)
        {
            Dictionary<string, object> status = GetStatus(lightId);
            if (lightId != null)
                light = account.GetSpecificLight(lightId);
            try
            {
                var lifxLight = light as LightBulb;
                UInt16 hueUse = Convert.ToUInt16(status["hue"]);
                UInt16 saturationUse = Convert.ToUInt16(status["saturation"]);
                double y1 = 1.0;
                double y2 = 65535;

                var stuff = (brightness * y2) / y1; 

                UInt16 outputBrightness = Convert.ToUInt16((brightness * y2) / y1); 
                UInt16 brightnessUse = outputBrightness; 
                UInt16 kelvinUse = Convert.ToUInt16(status["kelvin"]);


                lifxClient.SetStateAsync(lifxLight,
                                             hueUse,
                                             saturationUse,
                                             brightnessUse,
                                             kelvinUse,
                                             TimeSpan.Zero).GetAwaiter().GetResult();
                return "Used Lifx Client";
            }
            catch (Exception)
            {
                Dictionary<string, Object> brightnessDict = new Dictionary<string, Object>();
                float useBrightness = (float)brightness; 
                brightnessDict.Add("brightness", brightness);
                var json = light.BuildBrightnessJSONParams(brightnessDict).ToString();
                string toggle_url = connection.SetupUrl(base_url, light.id, "state");
                HttpClient client = connection.GetOrSetupClient(toggle_url, account.GetToken());
                HttpResponseMessage message = connection.HttpRequest(client, HttpMethod.Put, toggle_url, json).Result;
                string response = connection.ParseHTTPResponse(message).Result.ToString();
                connection.Close(connection);
                return response;
            }
        }

        /// <summary>
        /// Changes the temperate.
        /// </summary>
        /// <returns>The temperate.</returns>
        /// <param name="lightId">Light key.</param>
        /// <param name="kelvin">Kelvin [2500-9000]</param>
        public string ChangeTemperature(string lightId, int kelvin)
        {
            Dictionary<string, object> status = GetStatus(lightId);
            if (lightId != null)
                light = account.GetSpecificLight(lightId);
            try
            {
                var lifxLight = light as LightBulb;
                UInt16 hueUse = Convert.ToUInt16(status["hue"]);
                UInt16 saturationUse = Convert.ToUInt16(status["saturation"]);
                UInt16 brightnessUse = Convert.ToUInt16(status["brightness"]);
                UInt16 kelvinUse = Convert.ToUInt16(kelvin);

                lifxClient.SetStateAsync(lifxLight,
                                             hueUse,
                                             saturationUse,
                                             brightnessUse,
                                             kelvinUse,
                                             TimeSpan.Zero).GetAwaiter().GetResult();
                return "Used Lifx Client";
            }
            catch (Exception)
            {
                Color color = light.GetColor();
                var hue = color.hue;
                var brightness = color.brightness;
                var saturation = color.saturation;
                Dictionary<string, Object> colors = new Dictionary<string, Object>();
                Color newColor = new Color();
                newColor.CheckHSBKValues(hue, saturation, brightness, kelvin);
                newColor.hue = hue;
                newColor.brightness = brightness;
                newColor.saturation = saturation;
                newColor.kelvin = kelvin;
                colors.Add("color", newColor);
                var json = light.BuildTemperatureJSONParams(colors).ToString();
                string toggle_url = connection.SetupUrl(base_url, light.id, "state");
                HttpClient client = connection.GetOrSetupClient(toggle_url, account.GetToken());
                HttpResponseMessage message = connection.HttpRequest(client, HttpMethod.Put, toggle_url, json).Result;
                string response = connection.ParseHTTPResponse(message).Result.ToString();
                connection.Close(connection);
                return response;
            }
        }
    }
}
