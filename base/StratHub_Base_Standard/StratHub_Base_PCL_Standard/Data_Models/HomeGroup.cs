﻿using System;
using System.Collections.Generic; 
using StratHub_Base.LAN;
namespace StratHub_Base.Data_Models
{
    /// <summary>
    /// Group.
    /// </summary>
    internal class HomeGroup
    {
        private string id; 
        private Dictionary<string, Light> lanLights;
        private Dictionary<string, Light> httpLights;

        public HomeGroup(string id)
        {
            this.id = id;
            lanLights = new Dictionary<string, Light>(); 
            httpLights = new Dictionary<string, Light>(); 
        }

        /// <summary>
        /// Adds the HTTP Light.
        /// </summary>
        /// <param name="light">Light.</param>
        public void AddHTTPLight(Light light)
        {
            
            string lightId = light.id;
            if (!httpLights.ContainsKey(lightId))
                httpLights.Add(lightId, light);
        }

        /// <summary>
        /// Adds the HTTP Light.
        /// </summary>
        /// <param name="lightId">Light identifier.</param>
        /// <param name="light">Light.</param>
        public void AddHTTPLight(string lightId, Light light)
        {
            if (!httpLights.ContainsKey(lightId))
                httpLights.Add(lightId, light);
        }

        /// <summary>
        /// Adds the LAN Light.
        /// </summary>
        /// <param name="light">Light.</param>
        public void AddLANLight(Light light)
        {

            string lightId = light.id;
            if (!lanLights.ContainsKey(lightId))
                lanLights.Add(lightId, light);
        }

        /// <summary>
        /// Adds the LAN Light.
        /// </summary>
        /// <param name="lightId">Light identifier.</param>
        /// <param name="light">Light.</param>
        public void AddLANLight(string lightId, Light light)
        {
            if (!lanLights.ContainsKey(lightId))
                lanLights.Add(lightId, light);
        }

        /// <summary>
        /// Gets the specific light.
        /// </summary>
        /// <returns>The specific light.</returns>
        /// <param name="lightId">Light ID.</param>
        public Light GetSpecificLight(string lightId)
        {
            Light found_light = null;
            if (lanLights.ContainsKey(lightId))
            {
                lanLights.TryGetValue(lightId, out found_light);
            }
            else
            {
                httpLights.TryGetValue(lightId, out found_light);
            }
            return found_light;
        }

        /// <summary>
        /// Gets all LAN Lights.
        /// </summary>
        /// <returns>The all HTTPL ights.</returns>
        public Dictionary<string, Light> GetAllLANLights()
        {
            return lanLights;
        }

        /// <summary>
        /// Gets all HTTP Lights.
        /// </summary>
        /// <returns>The all HTTPL ights.</returns>
        public Dictionary<string, Light> GetAllHTTPLights()
        {
            return httpLights; 
        }

        /// <summary>
        /// Removes the light from the dictionaries.
        /// </summary>
        /// <param name="lightId">Light ID.</param>
        public void RemoveLight(string lightId)
        {
            if (httpLights.ContainsKey(lightId))
            {
                httpLights.Remove(lightId);
            }
            if (lanLights.ContainsKey(lightId))
            {
                lanLights.Remove(lightId); 
            }
        }

        /// <summary>
        /// Removes the light from the LAN dictionary.
        /// </summary>
        /// <param name="lightId">Light ID.</param>
        public void RemoveLANLight(string lightId)
        {
            if (lanLights.ContainsKey(lightId))
            {
                lanLights.Remove(lightId);
            }
        }

        /// <summary>
        /// Removes the lights from the dictionaries.
        /// </summary>
        /// <param name="light">Light.</param>
        public void RemoveLight(Light light)
        {
            if (httpLights.ContainsValue(light) && httpLights.ContainsKey(light.id))
            {
                httpLights.Remove(light.id); 
            }
            if (lanLights.ContainsValue(light) && lanLights.ContainsKey(light.id))
            {
                lanLights.Remove(light.id);
            }
        }

        /// <summary>
        /// Removes the lights from the LAN dictionaries.
        /// </summary>
        /// <param name="light">Light.</param>
        public void RemoveLANLight(Light light)
        {
            if (lanLights.ContainsValue(light) && lanLights.ContainsKey(light.id))
            {
                lanLights.Remove(light.id);
            }
        }

        /// <summary>
        /// Removes all lights from account.
        /// </summary>
        public void RemoveAllLightsFromHomeGroup()
        {
            httpLights.Clear(); 
            lanLights.Clear();
        }

        /// <summary>
        /// Removes all LAN lights from account.
        /// </summary>
        public void RemoveAllLANLightsFromHomeGroup()
        {
            lanLights.Clear();
        }
    }
}
