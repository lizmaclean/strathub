﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using StratHub_Base.IO_Models.ConnectionTypes;
using StratHub_Base.LAN;

namespace StratHub_Base.Data_Models
{
    /// <summary>
    /// Account Class.
    /// </summary>
    internal class Account
    {
        private static string user_name;
        private static string password;
        private static string token;
        private static Helper helper;
        private static Dictionary<string, Light> lanLights;
        private static Dictionary<string, Light> httpLights;
        private static Dictionary<string, HomeGroup> groups;

        private static Account _instance;


        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.Data_Models.Account"/> class.
        /// </summary>
        /// <param name="inToken">Token.</param>
        private Account(string inToken)
        {
            token = inToken;
        }

        /// <summary>
        /// Gets or makes account instance.
        /// </summary>
        /// <returns>The or make account.</returns>
        /// <param name="inToken">Token.</param>
        public static Account GetOrMakeAccount(string inToken)
        {
            if (_instance == null || token != _instance.GetToken())
            {
                token = inToken;
                user_name = "";
                password = "";
                helper = new Helper();
                lanLights = new Dictionary<string, Light>();
                httpLights = new Dictionary<string, Light>();
                groups = new Dictionary<string, HomeGroup>();
                _instance = new Account(token);
            }
            return _instance;
        }

        /// <summary>
        /// Gets or makes account instance.
        /// </summary>
        /// <returns>The or make account.</returns>
        /// <param name="inToken">Token.</param>
        /// <param name="inLights">Lights.</param>
        public static Account GetOrMakeAccount(string inToken, Dictionary<string, Light> inLights, bool isLanLights = true)
        {
            if (_instance == null || token != _instance.GetToken())
            {
                token = inToken;
                user_name = "";
                password = "";
                if (isLanLights)
                {
                    lanLights = inLights; 
                }
                else
                {
                    httpLights = inLights;    
                }
                helper = new Helper();
                groups = new Dictionary<string, HomeGroup>();
                _instance = new Account(token);
            }
            return _instance;
        }

        /// <summary>
        /// Gets or makes account instance.
        /// </summary>
        /// <returns>The or make account.</returns>
        /// <param name="inUserName">User name.</param>
        /// <param name="inPassword">Password.</param>
        /// <param name="inToken">Token.</param>
        public static Account GetOrMakeAccount(string inUserName, string inPassword, string inToken)
        {
            if (_instance == null || token != _instance.GetToken())
            {
                token = inToken;
                user_name = inUserName;
                password = inPassword;
                helper = new Helper();
                lanLights = new Dictionary<string, Light>();
                httpLights = new Dictionary<string, Light>();
                groups = new Dictionary<string, HomeGroup>();
                _instance = new Account(token);
            }
            return _instance;
        }

        /// <summary>
        /// Gets or makes account instance.
        /// </summary>
        /// <returns>The or make account.</returns>
        /// <param name="inUserName">User name.</param>
        /// <param name="inPassword">Password.</param>
        /// <param name="inToken">Token.</param>
        /// <param name="inLights">Lights.</param>
        public static Account GetOrMakeAccount(string inUserName, string inPassword, string inToken,
                        Dictionary<string, Light> inLights, bool isLanLights = true)
        {
            if (_instance == null || token != _instance.GetToken())
            {
                token = inToken;
                user_name = inUserName;
                password = inPassword;
                if (isLanLights)
                {
                    lanLights = inLights;
                }
                else
                {
                    httpLights = inLights;
                }
                helper = new Helper();
                groups = new Dictionary<string, HomeGroup>();
                _instance = new Account(token);
            }
            return _instance;
        }

        public async Task<List<Status>> ListLightsOnAccount(HTTPSConnection connection, string base_url)
        {
            HttpClient client = connection.GetOrSetupClient(base_url, token);
            Task<HttpResponseMessage> response = connection.HttpRequest(client, HttpMethod.Get, base_url);

            var results = new List<Status>(); 
            if (response.GetAwaiter().GetResult().Content.Headers.ContentLength.GetValueOrDefault() > 0)
            {
                var parsed_response = await connection.ParseHTTPResponse(response.Result);
                var myResult = helper.SetObjectVairables((JArray)parsed_response, new Status());
                //var myResult = await helper.SetObjectVairables((JObject)parsed_response, new Status());
                //results = (Status)myResult; 
                foreach(var result in myResult)
                {
                    results.Add((Status)result); 
                }
                //results = myResult;
            }
            else
            {
                Console.WriteLine(response.Result.StatusCode);
            }
            return results;
        }

        /// <summary>
        /// Adds the light to the light dictionary.
        /// </summary>
        /// <param name="light">Light.</param>
        public void AddLANLight(Light light)
        {
            string lightId = light.id;
            if (!lanLights.ContainsKey(lightId))
                lanLights.Add(lightId, light);
        }

        /// <summary>
        /// Adds the light to thr light dictionary.
        /// </summary>
        /// <param name="lightId">Light ID.</param>
        /// <param name="light">Light.</param>
        public void AddLANLight(string lightId, Light light)
        {
            if (!lanLights.ContainsKey(lightId))
                lanLights.Add(lightId, light);
        }

        /// <summary>
        /// Adds the HTTPL ight.
        /// </summary>
        /// <param name="light">Light.</param>
        public void AddHTTPLight(Light light)
        {
            string lightId = light.id;
            if (!httpLights.ContainsKey(lightId))
                httpLights.Add(lightId, light);
        }

        /// <summary>
        /// Adds the HTTPL ight.
        /// </summary>
        /// <param name="lightId">Light identifier.</param>
        /// <param name="light">Light.</param>
        public void AddHTTPLight(string lightId, Light light)
        {
            if (!httpLights.ContainsKey(lightId))
                httpLights.Add(lightId, light);
        }

        /// <summary>
        /// Adds the home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        public void AddHomeGroup(string group_name)
        {
            groups.Add(group_name, new HomeGroup(group_name));
        }

        /// <summary>
        /// Adds the HTTP Light to home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="light">Light.</param>
        public void AddLANLightToHomeGroup(string group_name, Light light)
        {
            if (groups.ContainsKey(group_name))
            {
                groups[group_name].AddLANLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Adds the HTTP Light to home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="light">Light.</param>
        public void AddHTTPLightToHomeGroup(string group_name, Light light)
        {
            if (groups.ContainsKey(group_name))
            {
                groups[group_name].AddHTTPLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Adds to home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="lightId">Light ID.</param>
        public void AddLANLightToHomeGroup(string group_name, string lightId)
        {
            if (groups.ContainsKey(group_name) && lanLights.ContainsKey(lightId))
            {
                Light light = lanLights[lightId];
                groups[group_name].AddLANLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Adds to home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="lightId">Light key.</param>
        public void AddHTTPLightToHomeGroup(string group_name, string lightId)
        {
            if (groups.ContainsKey(group_name) && httpLights.ContainsKey(lightId))
            {
                Light light = httpLights[lightId];
                groups[group_name].AddHTTPLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Gets the light token.
        /// </summary>
        /// <returns>The token.</returns>
        public string GetToken()
        {
            return token;
        }

        /// <summary>
        /// Gets the specific light.
        /// </summary>
        /// <returns>The specific light.</returns>
        /// <param name="lightId">Light ID.</param>
        public Light GetSpecificLight(string lightId)
        {
            Light found_light = null;
            if (lanLights.ContainsKey(lightId))
            {
                lanLights.TryGetValue(lightId, out found_light);
            }
            else
            {
                httpLights.TryGetValue(lightId, out found_light); 
            }


            //TODO: REMOVE THIS AFTER TESTING
            //httpLights.TryGetValue(lightId, out found_light); 

            return found_light;
        }

        /// <summary>
        /// Gets all LAN lights.
        /// </summary>
        /// <returns>The all lights.</returns>
        public Dictionary<string, Light> GetAllLANLights()
        {
            return lanLights;
        }

        /// <summary>
        /// Gets all HTTP lights.
        /// </summary>
        /// <returns>The all lights.</returns>
        public Dictionary<string, Light> GetAllHTTPLights()
        {
            return httpLights;
        }

        /// <summary>
        /// Removes the lan light from the dictionary.
        /// </summary>
        /// <param name="lightId">Light ID.</param>
        public void RemoveLANLight(string lightId)
        {
            if (lanLights.ContainsKey(lightId))
            {
                lanLights.Remove(lightId);
            }
        }

        /// <summary>
        /// Removes the http light from the dictionary.
        /// </summary>
        /// <param name="lightId">Light ID.</param>
        public void RemoveHTTPLight(string lightId)
        {
            if (httpLights.ContainsKey(lightId))
            {
                httpLights.Remove(lightId);
            }
        }

        /// <summary>
        /// Removes all lights from account.
        /// </summary>
        public void RemoveAllLANLightsFromAccount()
        {
            lanLights.Clear();
        }

        /// <summary>
        /// Removes all lights from account.
        /// </summary>
        public void RemoveAllHTTPLightsFromAccount()
        {
            httpLights.Clear();
        }

        /// <summary>
        /// Removes the home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        public void RemoveHomeGroup(string group_name)
        {
            groups.Remove(group_name);
        }

        /// <summary>
        /// Removes all home groups.
        /// </summary>
        public void RemoveAllHomeGroups()
        {
            groups.Clear();
        }

        /// <summary>
        /// Removes the light home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="light">Light.</param>
        public void RemoveLightHomeGroup(string group_name, Light light)
        {
            if (groups.ContainsKey(group_name))
            {
                groups[group_name].RemoveLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Removes the lan light from home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="lightId">Light ID.</param>
        public void RemoveLANLightFromHomeGroup(string group_name, string lightId)
        {
            if (groups.ContainsKey(group_name) && lanLights.ContainsKey(lightId))
            {
                Light light = lanLights[lightId];
                groups[group_name].RemoveLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Removes the http light from home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="lightId">Light ID.</param>
        public void RemoveHTTPLightFromHomeGroup(string group_name, string lightId)
        {
            if (groups.ContainsKey(group_name) && httpLights.ContainsKey(lightId))
            {
                Light light = httpLights[lightId];
                groups[group_name].RemoveLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Removes all lights from home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        public void RemoveAllLightsFromHomeGroup(string group_name)
        {
            if (groups.ContainsKey(group_name))
            {
                groups[group_name].RemoveAllLightsFromHomeGroup(); 
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Removes all LANL ights from home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        public void RemoveAllLANLightsFromHomeGroup(string group_name)
        {
            if (groups.ContainsKey(group_name))
            {
                groups[group_name].RemoveAllLANLightsFromHomeGroup();
            }
            else
            {
                //ERROR    
            }
        }
    }
}
