﻿using System; 
using System.Dynamic;

namespace StratHub_Base.Data_Models
{
    /// <summary>
    /// Statuses for the light.
    /// </summary>
    internal class Status : DynamicObject
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the UUID.
        /// </summary>
        /// <value>The UUID.</value>
        public string uuid { get; set; }

        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>The label.</value>
        public string label { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:StratHub_Base.Data_Models.Status"/> is connected.
        /// </summary>
        /// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
        public bool connected { get; set; }

        /// <summary>
        /// Gets or sets the power.
        /// </summary>
        /// <value>The power.</value>
        public string power { get; set; }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>The color.</value>
        public Color color { get; set; }

        /// <summary>
        /// Gets or sets the brightness.
        /// </summary>
        /// <value>The brightness.</value>
        public float brightness { get; set; }

        /// <summary>
        /// Gets or sets the group.
        /// </summary>
        /// <value>The group.</value>
        public Group group { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>The location.</value>
        public Location location { get; set; }

        /// <summary>
        /// Gets or sets the product.
        /// </summary>
        /// <value>The product.</value>
        public Product product { get; set; }

        /// <summary>
        /// Gets or sets the last seen.
        /// </summary>
        /// <value>The last seen.</value>
        public string last_seen{ get; set; }

        /// <summary>
        /// Gets or sets the seconds since seen.
        /// </summary>
        /// <value>The seconds since seen.</value>
        public int seconds_since_seen { get; set; }

        /// <summary>
        /// Ensures the brightness value falls within proper range.
        /// </summary>
        /// <returns>The brightness value.</returns>
        /// <param name="brightness">Brightness.</param>
        public static float EnsureBrightnessValue(float brightness)
        {
            float brightnessValue = 0.0f; 
            //Make sure we get values between 0.0 and 1.0; inclusive 
            if (brightness > 1)
            {
                brightnessValue = (float)(((int)brightness % 100) / 100.00); 
            }
            else if (brightness < 0) {
                throw new Exception("Brightness values are not within range.");
            }
            else {
                brightnessValue = brightness; 
            }
            return brightnessValue; 
        }
    }
}
