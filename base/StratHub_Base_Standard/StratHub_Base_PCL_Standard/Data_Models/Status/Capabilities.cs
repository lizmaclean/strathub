﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratHub_Base.Data_Models
{
    /// <summary>
    /// Capabilities Class.
    /// </summary>
    internal class Capabilities
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:StratHub_Base.Data_Models.Capabilities"/> has color.
        /// </summary>
        /// <value><c>true</c> if has color; otherwise, <c>false</c>.</value>
        public bool has_color { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:StratHub_Base.Data_Models.Capabilities"/> has
        /// variable color temp.
        /// </summary>
        /// <value><c>true</c> if has variable color temp; otherwise, <c>false</c>.</value>
        public bool has_variable_color_temp { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:StratHub_Base.Data_Models.Capabilities"/> has ir.
        /// </summary>
        /// <value><c>true</c> if has ir; otherwise, <c>false</c>.</value>
        public bool has_ir { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:StratHub_Base.Data_Models.Capabilities"/> has multizone.
        /// </summary>
        /// <value><c>true</c> if has multizone; otherwise, <c>false</c>.</value>
        public bool has_multizone { get; set; }
    }
}
