﻿using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Text;

using System.Diagnostics;

namespace StratHub_Base.IO_Models.ConnectionTypes
{
    internal class HTTPSConnection : Connection
    {
        private HttpClient client;
        private HttpRequestMessage http_request;
        private HttpContent http_content; 

        public HTTPSConnection()
        {
            
        }

        public HTTPSConnection(string ip_addr, int port)
        {
                
        }

        /// <summary>
        /// Close the specified connection_id.
        /// </summary>
        /// <returns>The close.</returns>
        /// <param name="connection_id">Connection identifier.</param>
        public override bool Close(object connection_id)
        {
            try
            {
                DisposeClient();
                return true; 
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;     
            }
        }

        /// <summary>
        /// Disposes the client.
        /// </summary>
        public void DisposeClient()
        {
            if (client != null)
            {
                client.CancelPendingRequests();
                client.Dispose();
                client = null;
            }
        }

        /// <summary>
        /// Open this instance.
        /// </summary>
        /// <returns>The open.</returns>
        public override object Open()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Read the specified connection_id.
        /// </summary>
        /// <returns>The read.</returns>
        /// <param name="connection_id">Connection identifier.</param>
        public override object Read(object connection_id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Write the specified sender_info and message.
        /// </summary>
        /// <returns>The write.</returns>
        /// <param name="sender_info">Sender info.</param>
        /// <param name="message">Message.</param>
        public override object Write(object sender_info, object message)
        {
            throw new NotImplementedException();
        }

        //TODO: Below Functions to above functions 
        /// <summary>
        /// Setups the URL to call for the light. 
        /// </summary>
        /// <returns>The URL.</returns>
        /// <param name="base_url">Base URL.</param>
        /// <param name="light_id">Light identifier.</param>
        /// <param name="api_call">API call.</param>
        public string SetupUrl(string base_url, string light_id, string api_call)
        {
            //Write strip functions that remove or detect / and puts one there if not
            string url = string.Format("{0}/{1}", string.Format(base_url, light_id), api_call);
            return url;
        }

        /// <summary>
        /// Gets the or setup client.
        /// </summary>
        /// <returns>The or setup client.</returns>
        /// <param name="url">URL.</param>
        /// <param name="token">Token.</param>
        public HttpClient GetOrSetupClient(string url, string token)
        {
            if (client == null)
            {
                client = new HttpClient();
                client.BaseAddress = SetupUri(url);
                client = SetupClientHeaders(client, token);
            }
            return client; 
        }

        //TODO: More modular or rename with JSON in name 
        /// <summary>
        /// Creates and sends the https the request based on the api URL.
        /// </summary>
        /// <returns>The request.</returns>
        /// <param name="client">Client.</param>
        /// <param name="method">Method.</param>
        /// <param name="url">URL.</param>
        public async Task<HttpResponseMessage> HttpRequest(HttpClient client, HttpMethod method, string url, string jsonMessage=null)
        {
            http_request = new HttpRequestMessage(method, url);
            http_request = SetupMessageHeaders(http_request);
            if (jsonMessage != null)
            {
                http_request.Content = SetupHttpConent(jsonMessage);
            }
            HttpResponseMessage response = null;

            if (method == HttpMethod.Get || method == HttpMethod.Post)
            {
                response = client.SendAsync(http_request).Result; 
            }
            else if (method == HttpMethod.Put)
            {
                response = client.PutAsync(http_request.RequestUri.AbsoluteUri, http_content).Result;
            }

            return response; 
        }

        /// <summary>
        /// Parses the HTTPR esponse.
        /// </summary>
        /// <returns>The HTTPR esponse.</returns>
        /// <param name="response">Response.</param>
        public async Task<Object> ParseHTTPResponse(HttpResponseMessage response)
        {
            //TODO: Work with more generics than JObject - more modular 
            string result = "";
            Object json = null;
            if (response.Content.Headers.ContentLength.GetValueOrDefault() > 0)
            {
                result = await response.Content.ReadAsStringAsync();
                Debug.WriteLine(result);

                if (JToken.Parse(result) is JArray)
                {
                    JArray json_array = JArray.Parse(result);
                    json = json_array; 
                    //json = JObject.Parse(json_array[0].ToString());
                }
                else
                {
                    //TODO: Figure out what to do here. 
                    json = result; 
                }
            }
            else
            {
                Debug.WriteLine(response.StatusCode);
            }
            return json;
        }

        /// <summary>
        /// Setups the URI.
        /// </summary>
        /// <returns>The URI.</returns>
        /// <param name="url">URL.</param>
        private Uri SetupUri(string url)
        {
            return new Uri(url);
        }

        /// <summary>
        /// Setups the client headers.
        /// </summary>
        /// <returns>The client headers.</returns>
        /// <param name="client">Client.</param>
        /// <param name="token">Token.</param>
        private HttpClient SetupClientHeaders(HttpClient client, string token)
        {
            //TODO: Make this more modular 
            client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", token));
            return client; 
        }

        /// <summary>
        /// Setups the message headers.
        /// </summary>
        /// <returns>The message headers.</returns>
        /// <param name="http_request">Http request.</param>
        private HttpRequestMessage SetupMessageHeaders(HttpRequestMessage http_request)
        {
            http_request.Headers.Add("Accept", "*/*");
            http_request.Headers.Add("ContentType", "application/json");
            return http_request; 
        }

        /// <summary>
        /// Setups the http conent.
        /// </summary>
        /// <returns>The http conent.</returns>
        /// <param name="jsonMessage">Json message.</param>
        private HttpContent SetupHttpConent(string jsonMessage)
        {
            //http_content = new StringContent(jsonMessage); 
            http_content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");
            http_content.Headers.Add("ContentType", "application/json");
            return http_content; 
        }
    }
}
