﻿using System;
namespace StratHub_Base.IO_Models
{
    /// <summary>
    /// Console class.
    /// </summary>
    internal class Console : IO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.IO_Models.Console"/> class.
        /// </summary>
        public Console()
        {
            
        }

        /// <summary>
        /// Read the specified connection_id.
        /// </summary>
        /// <returns>The read.</returns>
        /// <param name="connection_id">Connection identifier.</param>
        public object Read(object connection_id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Write the specified sender_info and message.
        /// </summary>
        /// <returns>The write.</returns>
        /// <param name="sender_info">Sender info.</param>
        /// <param name="message">Message.</param>
        public object Write(object sender_info, object message)
        {
            throw new NotImplementedException();
        }
    }
}
