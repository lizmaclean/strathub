﻿using System;
namespace StratHub_Base.IO_Models.FileTypes
{
    internal class TextFile : File
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.IO_Models.FileTypes.TextFile"/> class.
        /// </summary>
        public TextFile()
        {
        }

        /// <summary>
        /// Read the specified connection_id.
        /// </summary>
        /// <returns>The read.</returns>
        /// <param name="connection_id">Connection identifier.</param>
        public override object Read(object connection_id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Write the specified sender_info and message.
        /// </summary>
        /// <returns>The write.</returns>
        /// <param name="sender_info">Sender info.</param>
        /// <param name="message">Message.</param>
        public override object Write(object sender_info, object message)
        {
            throw new NotImplementedException();
        }
    }
}
