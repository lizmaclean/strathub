﻿using System;
namespace StratHub_Base.IO_Models
{
    /// <summary>
    /// File class.
    /// </summary>
    internal abstract class File : IO
    {
        /// <summary>
        /// Read the specified connection_id.
        /// </summary>
        /// <returns>The read.</returns>
        /// <param name="connection_id">Connection identifier.</param>
        public abstract Object Read(object connection_id);

        /// <summary>
        /// Write the specified sender_info and message.
        /// </summary>
        /// <returns>The write.</returns>
        /// <param name="sender_info">Sender info.</param>
        /// <param name="message">Message.</param>
        public abstract Object Write(object sender_info, object message);
    }
}
