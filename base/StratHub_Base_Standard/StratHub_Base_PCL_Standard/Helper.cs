﻿using System;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Collections.Generic; 

namespace StratHub_Base
{
    /// <summary>
    /// Helper class. 
    /// </summary>
    public class Helper
    {
        public Helper()
        {
            
        }

        /// <summary>
        /// Sets the object vairables based on dictionary key values pair. 
        /// </summary>
        /// <returns>The object vairables.</returns>
        /// <param name="results">Result you are using to populate the object in the form of a dictionary or json</param>
        public Object SetObjectVairables(Dictionary<String, Object> results, Object type)
        {
            foreach (var result in results)
            {
                var name = result.Key;
                PropertyInfo info = type.GetType().GetRuntimeProperty(name);
                info.SetValue(type, result.Value, null); 
            }
            return type;
        }

        
        /// <summary>
        /// Setting object variable list 
        /// </summary>
        /// <param name="results"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Object> SetObjectVairables(JArray results, Object type)
        {
            List<Object> object_variables = new List<Object>();
            var child_count = results.Count; 
            for (var i = 0; i < child_count; i++)
            {
                var json_obj = JObject.Parse(results[i].ToString());
                foreach (var result in json_obj)
                {
                    var name = result.Key;
                    PropertyInfo info = type.GetType().GetRuntimeProperty(name);
                    Type cast_type = info.PropertyType;
                    info.SetValue(type, result.Value.ToObject(cast_type), null);
                }
                object_variables.Add(type);
                Type input_type = type.GetType();
                type = Activator.CreateInstance(input_type); 
            }
            
            return object_variables;
        }


        /// <summary>
        /// Dynamically sets objects values based on the object async
        /// </summary>
        /// <param name="results">Result you are using to populate the object in the form of a dictionary or json</param>
        /// <param name="type">Type of object.</param>
        /// <param name="updateObj">Update object.</param>
        /// <param name="inPlace">If set to <c>true</c> in place.</param>
        public async Task<Object> SetObjectVairables(JObject results, Object type)
        {
            foreach (var result in results)
            {
                var name = result.Key;
                PropertyInfo info = type.GetType().GetRuntimeProperty(name);
                Type cast_type = info.PropertyType;
                info.SetValue(type, result.Value.ToObject(cast_type), null);
            }
            return type;
        }
    }
}
