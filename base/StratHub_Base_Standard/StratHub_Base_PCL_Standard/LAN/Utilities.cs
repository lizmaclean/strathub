﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratHub_Base.LAN
{
	internal static class Utilities
	{
		public static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Rgbs to hsb.
        /// </summary>
        /// <returns>The to hsb.</returns>
        /// <param name="inRed">In red.</param>
        /// <param name="inGreen">In green.</param>
        /// <param name="inBlue">In blue.</param>
        public static decimal[] RgbToHsb(int inRed, float inGreen, int inBlue)
        {
            decimal red = inRed / 255;
            decimal green = Convert.ToDecimal(inGreen / 255);
            decimal blue = inBlue / 255;

            decimal minValue = Math.Min(red, Math.Min(green, blue));
            decimal maxValue = Math.Max(red, Math.Max(green, blue));
            decimal delta = maxValue - minValue;

            decimal h = 0;
            decimal s = 0;
            decimal v = maxValue;

            if (maxValue == red)
            {
                if (green >= blue)
                {
                    if (delta == 0)
                    {
                        h = 0;
                    }
                    else
                    {
                        h = 60 * (green - blue) / delta;
                    }
                }
                else if (green < blue)
                {
                    h = 60 * (green - blue) / delta + 360;
                }
            }
            else if (maxValue == green)
            {
                h = 60 * (blue - red) / delta + 120;
            }
            else if (maxValue == blue)
            {
                h = 60 * (red - green) / delta + 240;
            }

            if (maxValue == 0)
            {
                s = 0; 
            }
            else
            {
                s = (decimal)1 - (minValue / maxValue); 
            }

            s *= 100;
            v *= 100;

            decimal[] hsb = new decimal[3];
            hsb[0] = Math.Round(h, MidpointRounding.AwayFromZero);
            hsb[1] = Math.Round(s, MidpointRounding.AwayFromZero);
            hsb[2] = Math.Round(v, MidpointRounding.AwayFromZero);

            return hsb; 
        }

        /// <summary>
        /// Rgbs to hsl.
        /// </summary>
        /// <returns>The to hsl.</returns>
        /// <param name="rgb">Rgb.</param>
		public static UInt16[] RgbToHsl(Color rgb)
		{
			// normalize red, green and blue values
			double r = (rgb.R / 255.0);
			double g = (rgb.G / 255.0);
			double b = (rgb.B / 255.0);

			double max = Math.Max(r, Math.Max(g, b));
			double min = Math.Min(r, Math.Min(g, b));

			double h = 0.0;
			if (max == r && g >= b)
			{
				h = 60 * (g - b) / (max - min);
			}
			else if (max == r && g < b)
			{
				h = 60 * (g - b) / (max - min) + 360;
			}
			else if (max == g)
			{
				h = 60 * (b - r) / (max - min) + 120;
			}
			else if (max == b)
			{
				h = 60 * (r - g) / (max - min) + 240;
			}

			double s = (max == 0) ? 0.0 : (1.0 - (min / max));
			return new UInt16[] {
				(UInt16)(h / 360 * 65535),
				(UInt16)(s * 65535),
				(UInt16)(max * 65535)
			};
		}


	}
}
