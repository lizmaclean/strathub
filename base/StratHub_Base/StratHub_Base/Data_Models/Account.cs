﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using StratHub_Base.IO_Models.ConnectionTypes;

namespace StratHub_Base.Data_Models
{
    /// <summary>
    /// Account Class.
    /// </summary>
    internal class Account
    {
        private string user_name;
        private string password;
        private string token;
        private Helper helper; 
        private Dictionary<string, Light> lights;
        private Dictionary<string, HomeGroup> groups; 

        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.Data_Models.Account"/> class.
        /// </summary>
        /// <param name="token">Token.</param>
        public Account(string token)
        {
            this.token = token;
            user_name = "";
            password = "";
            helper = new Helper(); 
            lights = new Dictionary<string, Light>();
            groups = new Dictionary<string, HomeGroup>(); 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.Data_Models.Account"/> class.
        /// </summary>
        /// <param name="token">Token.</param>
        /// <param name="lights">Lights.</param>
        public Account(string token, Dictionary<string, Light> lights)
        {
            this.token = token;
            this.lights = lights;
            user_name = "";
            password = "";
            helper = new Helper(); 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.Data_Models.Account"/> class.
        /// </summary>
        /// <param name="user_name">User name.</param>
        /// <param name="password">Password.</param>
        /// <param name="token">Token.</param>
        public Account(string user_name, string password, string token)
        {
            this.user_name = user_name;
            this.password = password;
            this.token = token;
            helper = new Helper(); 
            lights = new Dictionary<string, Light>(); 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.Data_Models.Account"/> class.
        /// </summary>
        /// <param name="user_name">User name.</param>
        /// <param name="password">Password.</param>
        /// <param name="token">Token.</param>
        /// <param name="lights">Lights.</param>
        public Account(string user_name, string password, string token,
                        Dictionary<string, Light> lights)
        {
            this.user_name = user_name;
            this.password = password;
            this.token = token;
            this.lights = lights;
            helper = new Helper(); 
        }

        /// <summary>
        /// Adds the light to the light dictionary.
        /// </summary>
        /// <param name="light">Light.</param>
        public void AddLight(Light light)
        {
            string key = light.id; 
            lights.Add(key, light);
        }

        /// <summary>
        /// Adds the light to thr light dictionary.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="light">Light.</param>
        public void AddLight(string key, Light light)
        {
            if (!lights.ContainsKey(key))
                lights.Add(key, light);
        }

        /// <summary>
        /// Listing all lights know to be on the LIFX Cloud
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="base_url"></param>
        /// <returns></returns>
        public async Task<List<Status>> ListLightsOnAccount(HTTPSConnection connection, string base_url)
        {
            HttpClient client = connection.GetOrSetupClient(base_url, token);
            Task<HttpResponseMessage> response = connection.HttpRequest(client, HttpMethod.Get, base_url);

            var results = new List<Status>();
            if (response.GetAwaiter().GetResult().Content.Headers.ContentLength.GetValueOrDefault() > 0)
            {
                var parsed_response = await connection.ParseHTTPResponse(response.Result);
                var myResult = helper.SetObjectVairables((JArray)parsed_response, new Status());
                //TODO: IS THIS RIGHT?
                foreach (var result in myResult)
                {
                    results.Add((Status)result);
                }
                //results = myResult;
            }
            else
            {
                Debug.WriteLine(response.Result.StatusCode);
            }
            return results;
        }

        /// <summary>
        /// Adds the home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        public void AddHomeGroup(string group_name)
        {
            groups.Add(group_name, new HomeGroup(group_name));
        }

        /// <summary>
        /// Adds to home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="light">Light.</param>
        public void AddLightToHomeGroup(string group_name, Light light)
        {
            if (groups.ContainsKey(group_name))
            {
                groups[group_name].AddLight(light);     
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Adds to home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="light_key">Light key.</param>
        public void AddLightToHomeGroup(string group_name, string light_key)
        {
            if (groups.ContainsKey(group_name) && lights.ContainsKey(light_key))
            {
                Light light = lights[light_key]; 
                groups[group_name].AddLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Gets the light token.
        /// </summary>
        /// <returns>The token.</returns>
        public string GetToken()
        {
            return token; 
        }

        /// <summary>
        /// Gets the specific light.
        /// </summary>
        /// <returns>The specific light.</returns>
        /// <param name="key">Key.</param>
        public Light GetSpecificLight(string key)
        {
            Light found_light = null;
            lights.TryGetValue(key, out found_light);
            return found_light;
        }

        /// <summary>
        /// Gets all lights.
        /// </summary>
        /// <returns>The all lights.</returns>
        public Dictionary<string, Light> GetAllLights()
        {
            return lights;
        }

        /// <summary>
        /// Removes the light from the dictionary.
        /// </summary>
        /// <param name="key">Key.</param>
        public void RemoveLight(string key)
        {
            if (lights.ContainsKey(key))
            {
                lights.Remove(key);    
            }
        }

        /// <summary>
        /// Removes all lights from account.
        /// </summary>
        public void RemoveAllLightsFromAccount()
        {
            lights.Clear();
        }

        /// <summary>
        /// Removes the home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        public void RemoveHomeGroup(string group_name)
        {
            groups.Remove(group_name);
        }

        /// <summary>
        /// Removes all home groups.
        /// </summary>
        public void RemoveAllHomeGroups()
        {
            groups.Clear();
        }

        /// <summary>
        /// Removes the light home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="light">Light.</param>
        public void RemoveLightHomeGroup(string group_name, Light light)
        {
            if (groups.ContainsKey(group_name))
            {
                groups[group_name].RemoveLight(light);
            }
            else
            {
                //ERROR    
            }
        }

        /// <summary>
        /// Removes the light from home group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="light_key">Light key.</param>
        public void RemoveLightFromHomeGroup(string group_name, string light_key)
        {
            if (groups.ContainsKey(group_name) && lights.ContainsKey(light_key))
            {
                Light light = lights[light_key];
                groups[group_name].RemoveLight(light);
            }
            else
            {
                //ERROR    
            }
        }
    }
}
