﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StratHub_Base.Data_Models
{
    /// <summary>
    /// Color Class.
    /// Basis: https://api.developer.lifx.com/docs/colors
    /// </summary>
    internal class Color
    {
        /// <summary>
        /// Gets or sets the Red values
        /// </summary>
        /// /// <value>Red Values.</value>
        public int red { get; set; }
        /// <summary>
        /// Gets or sets the green values
        /// </summary>
        /// /// <value>Green Values.</value>
        public int green { get; set; }
        /// <summary>
        /// Gets or sets the blue values
        /// </summary>
        /// /// <value>Blue Values.</value>
        public int blue { get; set; }
        /// <summary>
        /// Gets or sets the hue.
        /// </summary>
        /// <value>The hue.</value>
        public float hue { get; set; }
        /// <summary>
        /// Gets or sets the saturation.
        /// </summary>
        /// <value>The saturation.</value>
        public float saturation { get; set; }
        /// <summary>
        /// Gets or sets the kelvin.
        /// </summary>
        /// <value>The kelvin.</value>
        public int kelvin { get; set; }

        /// <summary>
        /// To Red Green Blue - RGB string. 
        /// </summary>
        /// <returns>The RGBS tring.</returns>
        public string ToRGBString()
        {
            return string.Format("rgb:{0},{1},{2}", red, green, blue);
        }

        /// <summary>
        /// To HEX value string 
        /// </summary>
        /// <returns>The HEXS tring.</returns>
        public string ToHEXString()
        {
            throw new NotImplementedException(); 
            //return string.Format("rgb:{0},{1},{2}", hue, saturation, kelvin); 
        }

        /// <summary>
        /// To Hue Saturation Kelvin - HSK string.
        /// </summary>
        /// <returns>The HSKS tring.</returns>
        public string ToHSKString()
        {
            return string.Format("hue:{0} saturation:{1} kelvin:{2}", hue, saturation, kelvin); 
        }

        /// <summary>
        /// Checks the HSK Values.
        /// </summary>
        /// <returns><c>true</c>, if HSKV alues was in range, <c>false</c> otherwise.</returns>
        /// <param name="hue">Hue.</param>
        /// <param name="saturation">Saturation.</param>
        /// <param name="kelvin">Kelvin.</param>
        public bool CheckHSKValues(float hue, float saturation, int kelvin)
        {
            if (saturation == 100)
            {
                saturation = 1; 
            }
            if ((hue < 0 || hue > 360) || 
                (saturation < 0 || saturation > 1) ||
                (kelvin < 1500 || kelvin > 9000))
            {
                throw new Exception("Color values are not within range.");
            }
            return true; 
        }

        /// <summary>
        /// Checks the RGB values.
        /// </summary>
        /// <returns><c>true</c>, if RGBV alue was in range, <c>false</c> otherwise.</returns>
        /// <param name="red">Red.</param>
        /// <param name="green">Green.</param>
        /// <param name="blue">Blue.</param>
        public bool CheckRGBValue(int red, int green, int blue)
        {
            if ((red < 0 || red > 255) ||
               (green < 0 || green > 255) ||
                (blue < 0 || blue > 255))
            {
                throw new Exception("Color values are not within range.");
            }
            return true; 
        }
    }
}
