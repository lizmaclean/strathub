﻿using System;
using System.Collections.Generic; 
namespace StratHub_Base.Data_Models
{
    /// <summary>
    /// Group.
    /// </summary>
    internal class HomeGroup
    {
        private string id; 
        private Dictionary<string, Light> lights;

        public HomeGroup(string id)
        {
            this.id = id;
            lights = new Dictionary<string, Light>(); 
        }

        /// <summary>
        /// Adds the light to the light dictionary.
        /// </summary>
        /// <param name="light">Light.</param>
        public void AddLight(Light light)
        {
            string key = light.id;
            lights.Add(key, light);
        }

        /// <summary>
        /// Adds the light to thr light dictionary.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="light">Light.</param>
        public void AddLight(string key, Light light)
        {
            lights.Add(key, light);
        }

        /// <summary>
        /// Gets the specific light.
        /// </summary>
        /// <returns>The specific light.</returns>
        /// <param name="key">Key.</param>
        public Light GetSpecificLight(string key)
        {
            Light found_light = null;
            lights.TryGetValue(key, out found_light);
            return found_light;
        }

        /// <summary>
        /// Gets all lights.
        /// </summary>
        /// <returns>The all lights.</returns>
        public Dictionary<string, Light> GetAllLights()
        {
            return lights; 
        }

        /// <summary>
        /// Removes the light from the dictionary.
        /// </summary>
        /// <param name="key">Key.</param>
        public void RemoveLight(string key)
        {
            if (lights.ContainsKey(key))
            {
                lights.Remove(key);
            }
        }

        /// <summary>
        /// Removes the light.
        /// </summary>
        /// <param name="light">Light.</param>
        public void RemoveLight(Light light)
        {
            if (lights.ContainsValue(light) && lights.ContainsKey(light.id))
            {
                lights.Remove(light.id); 
            }
        }

        /// <summary>
        /// Removes all lights from account.
        /// </summary>
        public void RemoveAllLightsFromHomeGroup()
        {
            lights.Clear();
        }
    }
}
