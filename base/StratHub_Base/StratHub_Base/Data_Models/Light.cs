﻿using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using StratHub_Base.IO_Models.ConnectionTypes;
using System.Collections.Generic; 

using System.Diagnostics;

namespace StratHub_Base.Data_Models
{
    /// <summary>
    /// Light class.
    /// </summary>
    internal class Light
    {
        private Helper helper; 

        /// <summary>
        /// Gets or sets the identifier. The key.
        /// </summary>
        /// <value>The identifier.</value>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        public string key { get; set; }

        /// <summary>
        /// Gets or sets the account token.
        /// </summary>
        /// <value>The account token.</value>
        public string account_token { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public Status status { get; set; }

        /// <summary>
        /// Gets or sets the groups belonged to.
        /// </summary>
        /// <value>The groups belonged to.</value>
        public Dictionary<string, string> groups_belonged_to { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:StratHub_Base.Data_Models.Light"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="account_token">Account token.</param>
        public Light(string id = "", string account_token = "")
        {
            helper = new Helper();
            this.id = id;
            this.account_token = account_token; 
        }

        /// <summary>
        /// Builds the JSON Params to send.
        /// </summary>
        /// <returns>The JSONP arams.</returns> NOTE: DO WE WANT THIS TO BE A STRING? 
        /// <param name="keyValues">Key values.</param>
        public JObject BuildJSONParams(Dictionary<string, Object> keyValues)
        {
            status = helper.SetObjectVairables(keyValues, status) as Status;
            JObject json = JObject.FromObject(new
            {
                power = status.power,
                color = status.color,
                brightness = status.brightness
            });
            return json; 
        }

        /// <summary>
        /// Builds the Color JSON Params to send.
        /// </summary>
        /// <returns>The JSONP arams.</returns>
        /// <param name="keyValues">Key values.</param>
        public JObject BuildColorJSONParams(Dictionary<string, Object> keyValues)
        {
            status = helper.SetObjectVairables(keyValues, status) as Status;
            JObject json = JObject.FromObject(new
            {
                power = status.power,
                color = status.color.ToRGBString(),
                brightness = status.brightness
            });
            return json;
        }

        /// <summary>
        /// Gets the status of the light from the light id.
        /// </summary>
        /// <returns>The status.</returns>
        /// <param name="connection">Connection.</param>
        /// <param name="base_url">Base URL.</param>
        /// <param name="light_id">Light identifier.</param>
        public async Task<EStatus> GetStatus(HTTPSConnection connection, string base_url, string light_id)
        {
            string status_url = connection.SetupUrl(base_url, light_id, "");
            HttpClient client = connection.GetOrSetupClient(status_url, account_token);
            Task<HttpResponseMessage> response = connection.HttpRequest(client, HttpMethod.Get, status_url); 

            EStatus result = EStatus.UNKNOWN;
            if (response.Result.Content.Headers.ContentLength.GetValueOrDefault() > 0)
            {
                var parsed_response = await connection.ParseHTTPResponse(response.Result);
                var status_result = helper.SetObjectVairables((JArray)parsed_response, new Status());
                var statusList = status_result;
                //TODO: Change this 
                this.status = (Status)statusList[0]; 
                if (!this.status.connected){
                    result = EStatus.DISCONNECTED;
                }
                else if (status.power == "on"){
                    result = EStatus.ON; 
                }
                else if(status.power == "off")
                {
                    result = EStatus.OFF; 
                }
            }
            else
            {
                Debug.WriteLine(response.Result.StatusCode);
            }
            return result;
        }

        /// <summary>
        /// Gets the color.
        /// </summary>
        /// <returns>The color.</returns>
        public Color GetColor()
        {
            return status.color; 
        }

        /// <summary>
        /// Adds the group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        /// <param name="data">Data.</param>
        public void AddGroup(string group_name, string data = "")
        {
            groups_belonged_to.Add(group_name, data);
        }

        /// <summary>
        /// Removes the group.
        /// </summary>
        /// <param name="group_name">Group name.</param>
        public void RemoveGroup(string group_name)
        {
            groups_belonged_to.Remove(group_name);
        }

        /// <summary>
        /// Removes from all groups.
        /// </summary>
        public void RemoveFromAllGroups()
        {
            groups_belonged_to.Clear();
        }
    }
}
