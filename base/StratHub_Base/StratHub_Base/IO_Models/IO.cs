﻿using System;
namespace StratHub_Base.IO_Models
{
    /// <summary>
    /// Input/Output class.
    /// </summary>
    internal interface IO
    {
        /// <summary>
        /// Write the specified message and sender_info.
        /// </summary>
        /// <returns>The write.</returns>
        /// <param name="message">Message.</param>
        /// <param name="sender_info">Sender info.</param>
        Object Write(Object message, Object sender_info = null);

        /// <summary>
        /// Read the specified connection_id.
        /// </summary>
        /// <returns>The read.</returns>
        /// <param name="connection_id">Connection identifier.</param>
        Object Read(Object connection_id = null);
    }
}
