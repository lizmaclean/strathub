﻿using System;
namespace StratHub_Base.IO_Models
{
    /// <summary>
    /// Connection class
    /// </summary>
    internal abstract class Connection : IO
    {
        /// <summary>
        /// Open this instance.
        /// </summary>
        /// <returns>The open.</returns>
        public abstract Object Open();
        /// <summary>
        /// Close the specified connection_id.
        /// </summary>
        /// <returns>The close.</returns>
        /// <param name="connection_id">Connection identifier.</param>
        public abstract bool Close(Object connection_id);
        /// <summary>
        /// Write the specified sender_info and message.
        /// </summary>
        /// <returns>The write.</returns>
        /// <param name="sender_info">Sender info.</param>
        /// <param name="message">Message.</param>
        public abstract Object Write(Object sender_info, Object message);
        /// <summary>
        /// Read the specified connection_id.
        /// </summary>
        /// <returns>The read.</returns>
        /// <param name="connection_id">Connection identifier.</param>
        public abstract Object Read(Object connection_id);


    }
}
