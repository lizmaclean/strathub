﻿using System.Collections.Generic;
using System.Net.Http;
using StratHub_Base.Data_Models;
using StratHub_Base.IO_Models.ConnectionTypes;

namespace StratHub_Base
{
    /// <summary>
    /// API Class 
    /// </summary>
    public class StratHubBase
    {
        private static StratHubBase _instance;

        private readonly string _baseUrl;
        private Account _account;
        private Light _light;
        private readonly HTTPSConnection _connection;

        //Make lifx account an object
        //Make light an object 

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:StratHub_Base.StratHub_Base" /> class.
        /// </summary>
        private StratHubBase(string token)
        {
            _baseUrl = "https://api.lifx.com/v1/lights/id:{0}";
            _connection = new HTTPSConnection();
            GetOrMakeAccount(token);
            DiscoverLights();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:StratHub_Base.StratHub_Base" /> class.
        /// </summary>
        /// <param name="base_url">Base URL.</param>
        private StratHubBase(string token, string base_url)
        {
            this._baseUrl = base_url;
            GetOrMakeAccount(token);
            _connection = new HTTPSConnection();
        }

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        /// <returns>The instance.</returns>
        /// <param name="base_url">Base URL.</param>
        public static StratHubBase GetInstance(string token, string base_url = null)
        {
            if (_instance == null)
            {
                if (base_url != null)
                    _instance = new StratHubBase(token, base_url);
                else
                    _instance = new StratHubBase(token);
            }

            return _instance;
        }

        /// <summary>
        ///     Discovers the lights.
        /// </summary>
        private void DiscoverLights()
        {
            var list_lights_url = "https://api.lifx.com/v1/lights/all";
            var statuses = _account.ListLightsOnAccount(_connection, list_lights_url).Result;
            foreach (var status in statuses) AddLight(_account.GetToken(), status.id, status.id, status);
        }

        /// <summary>
        ///     Gets the or make account to use.
        /// </summary>
        /// <returns>The or make account.</returns>
        /// <param name="token">Token.</param>
        internal Account GetOrMakeAccount(string token)
        {
            if (_account == null || token != _account.GetToken()) _account = new Account(token);
            return _account;
        }

        /// <summary>
        ///     Adds the light.
        /// </summary>
        /// <param name="account_token">Account token.</param>
        /// <param name="light_id">Light identifier.</param>
        /// <param name="light_key">Light key.</param>
        /// <param name="group">Home Group.</param>
        private void AddLight(string account_token, string light_id, string light_key, Status status,
            string group_name = null)
        {
            _account = GetOrMakeAccount(account_token);
            _light = new Light();
            _light.id = light_id;
            _light.key = light_key;
            _light.status = status;
            _light.account_token = _account.GetToken();
            _account.AddLight(light_key, _light);
            if (!string.IsNullOrEmpty(group_name)) _account.AddHomeGroup(group_name);
        }

        /// <summary>
        ///     Sets the light to use
        /// </summary>
        /// <param name="light_key"></param>
        public void UseLight(string light_key)
        {
            _light = _account.GetSpecificLight(light_key);
        }

        /// <summary>
        ///     Returns true if light it on, false if light is off
        /// </summary>
        /// <param name="lightId"></param>
        /// <returns></returns>
        public bool IsLightOn(string lightId = null)
        {
            if (lightId != null) UseLight(lightId);
            if (_light.status == null)
                return false;
            if (_light.status.power == "on")
                return true;
            return false;
        }

        /// <summary>
        ///     Gets all the light keys for selection
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllLights()
        {
            var lightKeys = new List<string>();
            var lights = _account.GetAllLights();
            foreach (var kvLight in lights) lightKeys.Add(kvLight.Key);
            return lightKeys;
        }


        /// <summary>
        ///     Gets the status. With keys: power, hue, brightness, kelvin, saturation
        /// </summary>
        /// <returns>The status dictionary. With keys: power, hue, brightness, kelvin, saturation</returns>
        /// <param name="light_key">Light key.</param>
        public Dictionary<string, object> GetStatus(string light_key = null)
        {
            if (light_key != null)
                _light = _account.GetSpecificLight(light_key);
            var eStatus = _light.GetStatus(_connection, _baseUrl, _light.id).Result;
            var status_obj = _light.status;
            var status = new Dictionary<string, object>();
            _connection.Close(_connection);
            status.Add("power", eStatus);
            status.Add("hue", status_obj.color.hue);
            status.Add("brightness", status_obj.brightness);
            status.Add("kelvin", status_obj.color.kelvin);
            status.Add("saturation", status_obj.color.saturation);
            return status;
        }

        /// <summary>
        ///     Toggles the power.
        /// </summary>
        /// <returns>The power.</returns>
        /// <param name="light_key">Light key.</param>
        public string TogglePower(string light_key = null)
        {
            GetStatus(light_key);
            if (light_key != null)
                _light = _account.GetSpecificLight(light_key);
            var toggle_url = _connection.SetupUrl(_baseUrl, _light.id, "toggle");
            var client = _connection.GetOrSetupClient(toggle_url, _account.GetToken());
            var message = _connection.HttpRequest(client, HttpMethod.Post, toggle_url).Result;
            var response = _connection.ParseHTTPResponse(message).Result.ToString();
            _connection.Close(_connection);
            return response;
        }

        //TODO: Check this method and see if you can simplify and make more generic 
        /// <summary>
        ///     Changes the color of the light.
        /// </summary>
        /// <returns>The color.</returns>
        /// <param name="light_key">Light key.</param>
        /// <param name="hueORRed">Hue [0-360] or Red [0-255].</param>
        /// <param name="saturationOrGreen">Saturation [0.0-1.0] or green [0-255].</param>
        /// <param name="kelvinOrBlue">Kelvin [1500-9000]or blue [0-255].</param>
        /// <param name="isRGB">If set to <c>true</c> is rgb.</param>
        public string ChangeColor(string light_key, int hueORRed, float saturationOrGreen, int kelvinOrBlue,
            bool isRGB = false)
        {
            GetStatus(light_key);
            if (light_key != null)
                _light = _account.GetSpecificLight(light_key);
            var colors = new Dictionary<string, object>();
            var newColor = new Color();
            string json = ""; 
            if (isRGB)
            {
                newColor.CheckRGBValue(hueORRed, (int)saturationOrGreen, kelvinOrBlue);
                newColor.red = hueORRed;
                newColor.green = (int)saturationOrGreen;
                newColor.blue = kelvinOrBlue;
                colors.Add("color", newColor);
                json = _light.BuildColorJSONParams(colors).ToString(); 
            }
            else
            {
                newColor.CheckHSKValues(hueORRed, saturationOrGreen, kelvinOrBlue);
                newColor.hue = hueORRed;
                newColor.saturation = saturationOrGreen;
                newColor.kelvin = kelvinOrBlue;
                colors.Add("color", newColor);
                json = _light.BuildJSONParams(colors).ToString();
            }
            
            var toggle_url = _connection.SetupUrl(_baseUrl, _light.id, "state");
            var client = _connection.GetOrSetupClient(toggle_url, _account.GetToken());
            var message = _connection.HttpRequest(client, HttpMethod.Put, toggle_url, json).Result;
            var response = _connection.ParseHTTPResponse(message).Result.ToString();
            _connection.Close(_connection);
            return response;
        }

        /// <summary>
        ///     Changes the brightness.
        /// </summary>
        /// <returns>The brightness.</returns>
        /// <param name="light_key">Light key.</param>
        /// <param name="brightness">Brightness.</param>
        public string ChangeBrightness(string light_key, float brightness)
        {
            GetStatus(light_key);
            if (light_key != null)
                _light = _account.GetSpecificLight(light_key);
            var brightnessDict = new Dictionary<string, object>();
            brightness = Status.EnsureBrightnessValue(brightness);
            brightnessDict.Add("brightness", brightness);
            var json = _light.BuildJSONParams(brightnessDict).ToString();
            var toggle_url = _connection.SetupUrl(_baseUrl, _light.id, "state");
            var client = _connection.GetOrSetupClient(toggle_url, _account.GetToken());
            var message = _connection.HttpRequest(client, HttpMethod.Put, toggle_url, json).Result;
            var response = _connection.ParseHTTPResponse(message).Result.ToString();
            _connection.Close(_connection);
            return response;
        }

        /// <summary>
        ///     Changes the temperate.
        /// </summary>
        /// <returns>The temperate.</returns>
        /// <param name="light_key">Light key.</param>
        /// <param name="kelvin">Kelvin.</param>
        public string ChangeTemperature(string light_key, int kelvin)
        {
            GetStatus(light_key);
            if (light_key != null)
                _light = _account.GetSpecificLight(light_key);
            var color = _light.GetColor();
            var hue = color.hue;
            var saturation = color.saturation;
            var colors = new Dictionary<string, object>();
            var newColor = new Color();
            newColor.CheckHSKValues(hue, saturation, kelvin);
            newColor.hue = hue;
            newColor.saturation = saturation;
            newColor.kelvin = kelvin;
            colors.Add("color", newColor);
            var json = _light.BuildJSONParams(colors).ToString();
            var toggleUrl = _connection.SetupUrl(_baseUrl, _light.id, "state");
            var client = _connection.GetOrSetupClient(toggleUrl, _account.GetToken());
            var message = _connection.HttpRequest(client, HttpMethod.Put, toggleUrl, json).Result;
            var response = _connection.ParseHTTPResponse(message).Result.ToString();
            _connection.Close(_connection);
            return response;
        }
    }
}