﻿using System;
using StratHub_Base; 
using System.Collections.Generic;
using System.Xml.Linq;
using System.Threading; 

namespace Senior_Design_Run
{
    class MainClass
    {
        static string token = "c4ee25616303ac1c55af8037fafe08ed03064813fc0acc0d3d9a01683df18f3c";
        static string light_id = "d073d52360de";
        static string light_key = "LIFX A19 2360DE";
        static string response = ""; 
        static Dictionary<string, object> status = new Dictionary<string, object>();

        private static StratHub_Base.StratHub_Base base_cls = new StratHub_Base.StratHub_Base(token);

        /// <summary>
        /// Main function that opens a console interface
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        public static void Main(string[] args)
        {
            string message = "Please enter the number corresponding to the " +
                              "desired action: \n" +
                              "1. - Toggle Light\n" +
                              "2. - Cycle Colors\n" +
                              "3. - Get Status\n" +
                              "4. - Cycle Brightness\n" +
                              "5. - Cycle Temperature\n" +
                              "6. - Exit\n" +
                              "\nResponse: "; 
            Console.Write(message);

            string input = Console.ReadLine();
            int selection = -1; 
            while (true)
            {
                int.TryParse(input, out selection);
                if (selection == 6)
                {
                    break;
                }
                else if (selection > 0 && selection < 7)
                {
                    Run_Action(selection);   
                }
                Console.Write(message);
                input = Console.ReadLine();
            }
        }

        /// <summary>
        /// Runs the action specified by the selection integer.  
        /// </summary>
        /// <param name="selection">From the menu that selects what to run</param>
        public static void Run_Action(int selection)
        {
            switch (selection)
            {
                case 1:
                    Toggle_Light(); 
                    Get_Status();
                    break;
                case 2:
                    CycleColors();
                    Get_Status(); 
                    break; 
                case 3:
                    Get_Status();
                    break;
                case 4:
                    CycleBrightness();
                    break; 
                case 5:
                    CycleTemperature(); 
                    break; 
                default:
                    Get_Status();
                    break;
            }
        }

        /// <summary>
        /// Gets the status of the light and dyanmically places the information into a Status Object
        /// </summary>
        public static void Get_Status()
        {
            status = base_cls.GetStatus(light_id);
            string ret_val = string.Format("Status: {0}\n", status["power"]); 
            Console.WriteLine(ret_val);
        }

        /// <summary>
        /// Toggles the light power.
        /// </summary>
        public static void Toggle_Light()
        {
            response = base_cls.TogglePower(light_id);
            string ret_val = string.Format("Http Response: {0}", response);
            Console.WriteLine(ret_val);
        }

        /// <summary>
        /// Cycles the colors.
        /// </summary>
        public static void CycleColors()
        {
            ChangeColorRed();
            Thread.Sleep(1000);
            ChangeColorGreen();
            Thread.Sleep(1000);
            ChangeColorPurple();
        }

        /// <summary>
        /// Cycles the brightness.
        /// </summary>
        public static void CycleBrightness()
        {
            ChangeBrightnessTo1();
            Thread.Sleep(1000);
            ChangeBrightnessTo50();
            Thread.Sleep(1000);
            ChangeBrightnessTo1();
        }

        /// <summary>
        /// Cycles the temperature.
        /// </summary>
        public static void CycleTemperature()
        {
            ChangeTemperatureToWarm();
            Thread.Sleep(1000);
            ChangeTemperatureToCool();
            Thread.Sleep(1000);
            ChangeTemperatureToWarm();
        }

        /// <summary>
        /// Changes the color red.
        /// </summary>
        public static void ChangeColorRed()
        {
            response = base_cls.ChangeColor(light_id, 186, 16, 16);

            string ret_val = string.Format("Http Response: {0}", response);
            Console.WriteLine(ret_val);
        }

        /// <summary>
        /// Changes the color green.
        /// </summary>
        public static void ChangeColorGreen()
        {
            response = base_cls.ChangeColor(light_id, 24, 183, 12);

            string ret_val = string.Format("Http Response: {0}", response);
            Console.WriteLine(ret_val);
        }

        /// <summary>
        /// Changes the color purple.
        /// </summary>
        public static void ChangeColorPurple()
        {
            response = base_cls.ChangeColor(light_id, 119, 66, 244); 

            string ret_val = string.Format("Http Response: {0}", response);
            Console.WriteLine(ret_val);
        }

        /// <summary>
        /// Changes the brightness to 1% cannot use 1.
        /// </summary>
        public static void ChangeBrightnessTo1()
        {
            response = base_cls.ChangeBrightness(light_id, (float)0.1);

            string ret_val = string.Format("Http Response: {0}", response);
            Console.WriteLine(ret_val);
        }

        /// <summary>
        /// Changes the brightness to 50.
        /// </summary>
        public static void ChangeBrightnessTo50()
        {
            response = base_cls.ChangeBrightness(light_id, (float)0.5);

            string ret_val = string.Format("Http Response: {0}", response);
            Console.WriteLine(ret_val);
        }

        /// <summary>
        /// Changes the temperature to cool.
        /// </summary>
        public static void ChangeTemperatureToCool()
        {
            response = base_cls.ChangeTemperature(light_id, 8000);

            string ret_val = string.Format("Http Response: {0}", response);
            Console.WriteLine(ret_val);
        }

        /// <summary>
        /// Changes the temperature to warm.
        /// </summary>
        public static void ChangeTemperatureToWarm()
        {
            response = base_cls.ChangeTemperature(light_id, 2700);

            string ret_val = string.Format("Http Response: {0}", response);
            Console.WriteLine(ret_val);
        }
    }
}
