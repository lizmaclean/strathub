﻿using System;
using System.Net.Http;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

using System.Collections.Generic;

using System.Diagnostics;

namespace Senior_Design
{
    public class MyClass
    {
        private HttpClient client;
        private string token;
        private string light_id;
        private string base_url;
        private HttpRequestMessage http_request; 
     
        //Make lifx account an object
        //Make light an object 

        public MyClass()
        {
            client = new HttpClient();
            token = "c104ab1de25c82ad805679b02ed428e36697f4539545991b6907bec229a6d424";
            light_id = "d073d52360de";
            base_url = "https://api.lifx.com/v1/lights/id:{0}";
        }

        public MyClass(Dictionary<object, object> settings)
        {
            client = new HttpClient();
            token = "c104ab1de25c82ad805679b02ed428e36697f4539545991b6907bec229a6d424";
            light_id = "d073d52360de";
            base_url = "https://api.lifx.com/v1/lights/id:{0}";
        }

        public async void Run()
        {
            string toggle_url = SetupUrl(base_url, light_id, "toggle");
            SetupClient(toggle_url);
            HttpResponseMessage message = await HttpRequest(HttpMethod.Post, toggle_url);

            string status = await GetStatus(); 
            
        }

        private string SetupUrl(string base_url, string light_id, string api_call)
        {
            //Write strip functions that remove or detect / and puts one there if not
            string url = string.Format("{0}/{1}", string.Format(base_url, light_id), api_call);
            return url; 
        }

        private Uri SetupUri(string url)
        {
            return new Uri(url); 
        }

        private void SetupClient(string url)
        {
            client = new HttpClient();
            client.BaseAddress = SetupUri(url);
            client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", token));
        }

        private void DisposeClient()
        {
            if (client != null)
            {
                client.CancelPendingRequests();
                client.Dispose();
                client = null;
            }
        }

        private async Task<HttpResponseMessage> HttpRequest(HttpMethod method, string url)
        {
            http_request = new HttpRequestMessage(method, url);
            http_request.Headers.Add("Accept", "*/*");
            http_request.Headers.Add("ContentType", "application/json");
            HttpResponseMessage response = client.SendAsync(http_request).Result;
            DisposeClient(); 
            return response; 
        }

        private async Task<string> GetStatus()
        {
            string status_url = SetupUrl(base_url, light_id, "");
            SetupClient(status_url);
            Task<HttpResponseMessage> response = HttpRequest(HttpMethod.Get, status_url);

            string result = "";
            if (response.Result.Content.Headers.ContentLength.GetValueOrDefault() > 0)
            {
                var parsed_response = ParseHTTPResponse(response.Result);
                var status_result = await SetObjectVairables((JObject)parsed_response.Result, new Status());
                Status status = status_result as Status;
                result = status.power;
                Debug.WriteLine(result); 
            }
            else
            {
                Debug.WriteLine(response.Result.StatusCode);
            }
            return result;
        }

        private async Task<Object> ParseHTTPResponse(HttpResponseMessage response)
        {
            string result = "";
            JObject json = null; 
            if (response.Content.Headers.ContentLength.GetValueOrDefault() > 0)
            {
                result = await response.Content.ReadAsStringAsync();
                Debug.WriteLine(result);
                
                JArray json_array = JArray.Parse(result);
                json = JObject.Parse(json_array[0].ToString());
            }
            else
            {
                Debug.WriteLine(response.StatusCode);
            }
            //return result; 
            return json;
        }

        /// <summary>
        /// Dynamically sets objects values based on the object 
        /// </summary>
        /// <param name="result">Result you are using to populate the object in the form of a dictionary or json</param>
        /// <param name="type">Object you wish to populate.</param>
        /// <returns>Populated Object</returns>
        private async Task<Object> SetObjectVairables(JObject results, Object type)
        {
            foreach (var result in results)
            {
                var name = result.Key;
                PropertyInfo info = type.GetType().GetRuntimeProperty(name);
                Type cast_type = info.PropertyType; 
                info.SetValue(type, result.Value.ToObject(cast_type), null);
            }
            return type; 
        }
    }
}
