﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using Senior_Design.Data_Objects;

namespace Senior_Design
{
    class Status : DynamicObject
    {
        public string id { get; set; }
        public string uuid { get; set; }
        public string label { get; set; }
        public bool connected { get; set; }
        public string power { get; set; }
        public Color color { get; set; }
        public float brightness { get; set; }
        public Group group { get; set; }
        public Location location { get; set; }
        public Product product { get; set; }
        public string last_seen{ get; set; }
        public int seconds_since_seen { get; set; }
    }
}
