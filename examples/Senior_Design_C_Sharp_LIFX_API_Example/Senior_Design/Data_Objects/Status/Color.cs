﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Senior_Design.Data_Objects
{
    class Color
    {
        public float hue { get; set; }
        public float saturation { get; set; }
        public int kelvin { get; set; }
    }
}
