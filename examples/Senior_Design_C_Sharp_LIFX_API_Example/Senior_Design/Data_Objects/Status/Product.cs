﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Senior_Design.Data_Objects
{
    class Product
    {
        public string name { get; set; }
        public string identifer { get; set; }
        public string company { get; set; }
        public Capabilities capabilities { get; set; }
    }
}
